import java.util.ArrayList;
import java.util.List;

public class Questionnaire {
    

    private int idQuestionnaire;
    private String nomQuestionnaire;
    private Character etat;
    private int createur;
    private int idPan;
    private int numClient;
    private List<Question> listeQuestions; 


    public Questionnaire(int idQuestionnaire, String nomQuestionnaire,char etat,int numClient, int createur, int idPan){
        this.idQuestionnaire = idQuestionnaire;
        this.nomQuestionnaire = nomQuestionnaire;
        this.etat = etat;
        this.numClient = numClient;
        this.createur = createur;
        this.idPan = idPan; 
        this.listeQuestions = new ArrayList<>();
    }

    public Questionnaire(int idQuestionnaire, String nomQuestionnaire,int numClient, int createur, int idPan){
        this(idQuestionnaire,nomQuestionnaire,'C',numClient,createur,idPan);
    }

    // Les Getters
    public Character getEtat() {
        return etat;
    }

    public int getIdQuestionnaire() {
        return idQuestionnaire;
    }

    public List<Question> getListeQuestions() {
        return listeQuestions;
    }

    public String getNomQuestionnaire() {
        return nomQuestionnaire;
    }

    public void ajouteQuestion(Question question){
        this.listeQuestions.add(question);
    }
    public int getIdCreateur(){
        return this.createur;
    }

    public int getNumClient(){
        return this.numClient;
    }

    public int getidPan(){
        return this.idPan;
    }
    //Les Setters
    public void setEtat(Character etat) {
        this.etat = etat;
    }

    public void setIdQuestionnaire(int idQuestionnaire) {
        this.idQuestionnaire = idQuestionnaire;
    }

    public void setListeQuestions(List<Question> listeQuestions) {
        this.listeQuestions = listeQuestions;
    }

    public void setNomQuestionnaire(String nomQuestionnaire) {
        this.nomQuestionnaire = nomQuestionnaire;
    }

    @Override
    public String toString(){
        String etatQ = "";
        if(this.etat == 'C'){
            etatQ += " en cours de conception.";
        }
        else{
            etatQ += " terminé.";
        }
        String res = this.idQuestionnaire + ", " + this.nomQuestionnaire + ", et est" + etatQ + " Liste des quetions :\n";
        for(Question q : this.listeQuestions){
            res += q + "\n";
        }
        return res;
    }
}
