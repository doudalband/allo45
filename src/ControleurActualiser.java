import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurActualiser implements EventHandler<ActionEvent>{
    

    private AppliPlusieursFenetres appli;
    private FenetreSondeur fenetreSondeur;

    public ControleurActualiser(AppliPlusieursFenetres appli, FenetreSondeur fenetreSondeur){
        this.appli = appli;
        this.fenetreSondeur = fenetreSondeur;
    }

    @Override
    public void handle(ActionEvent event){
        this.fenetreSondeur.mettreLesSondageRecherche(this.appli.chargerSondage());

    }
}
