import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurCreerClient implements EventHandler<ActionEvent> {
    private FenetreConcepteur fenetreConcepteur;
    
    public ControleurCreerClient(FenetreConcepteur fenetreConcepteur) {
        this.fenetreConcepteur = fenetreConcepteur;
    }

    @Override
    public void handle(ActionEvent event) {
        this.fenetreConcepteur.setCentreAjouterClient();

        
    }

}
