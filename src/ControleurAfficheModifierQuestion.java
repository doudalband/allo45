import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurAfficheModifierQuestion implements EventHandler<ActionEvent> {

    private FenetreConcepteur fenetreConcepteur;
    private Question question;

    public ControleurAfficheModifierQuestion(FenetreConcepteur fenetreConcepteur, Question question) {
        this.fenetreConcepteur = fenetreConcepteur;
        this.question = question;
    }

    @Override
    public void handle(ActionEvent arg0) {
        this.fenetreConcepteur.setCentreModifierQuestion(question);
    }

}
