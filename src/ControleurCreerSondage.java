import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurCreerSondage implements EventHandler<ActionEvent>{
    

    private FenetreConcepteur fenetreConcepteur;
    private AppliPlusieursFenetres application;
    private ListeClients modele;

    public ControleurCreerSondage(FenetreConcepteur fenetreConcepteur,AppliPlusieursFenetres application,ListeClients modele){
        this.fenetreConcepteur = fenetreConcepteur;
        this.application = application;
        this.modele = modele;
    }

    @Override
    public void handle(ActionEvent event){
        RequeteSQL.creerSondage(this.application, this.fenetreConcepteur, this.modele);
            
    }
    
}
