public class ValPossible {
    
    private int idV;
    private String valeur;

    public ValPossible(int idV, String valeur){
        this.idV = idV;
        this.valeur = valeur;
    }

    public int getIdV() {
        return idV;
    }

    public String getValeur() {
        return valeur;
    }

    public void setIdV(int idV) {
        this.idV = idV;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }
    @Override
    public String toString(){
        return this.valeur+" "+this.idV;
    }
}