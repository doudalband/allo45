import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ControleurRepondre implements EventHandler<ActionEvent>{

    FenetreSondeur vueSondeur;
    Map<Question, List<Node>> dico;
    AppliPlusieursFenetres appli;


    public ControleurRepondre(AppliPlusieursFenetres appli, FenetreSondeur vueSondeur) {
        this.vueSondeur = vueSondeur;
        this.appli = appli;
        this.dico = this.vueSondeur.getDicoReponse(); 

    }

    @Override
    public void handle(ActionEvent actionEvent) {
        boolean repond = true;
        Map<Question, String> reponse = new HashMap<>();
        for (Map.Entry<Question, List<Node>> mapentry : this.dico.entrySet()){
            String res = "";
            if (mapentry.getKey().getTypeReponse().equals('l')){
                TextField tf = (TextField) mapentry.getValue().get(0);
                res=tf.getText();
                mapentry.getKey().getListeReponse().add(res);
            }
            
            else if (mapentry.getKey().getTypeReponse().equals('n')){
                ComboBox<Integer> cb = (ComboBox<Integer>) mapentry.getValue().get(0);
                try{
                    res = cb.getValue().toString();
                    mapentry.getKey().getListeReponse().add(res);
                }
                catch(NullPointerException e){
                    ;
                }
                               
            }

            else if (mapentry.getKey().getTypeReponse().equals('u')){
                for (Node n: mapentry.getValue()){
                    RadioButton rb = (RadioButton) n;
                    if(rb.isSelected()){
                        for (ValPossible v: mapentry.getKey().getListeValPossible()){
                            if (v.getValeur().equals(rb.getText())){
                                res = Integer.toString(v.getIdV());
                                mapentry.getKey().getListeReponse().add(res);
                            }
                        }
                    }
                }
                
            }
            else if (mapentry.getKey().getTypeReponse().equals('m')){
                int i =1;
                for (Node n: mapentry.getValue()){
                    CheckBox cb = (CheckBox) n;
                    if(cb.isSelected()){
                        res += i+";";  
                    }
                    i+=1;
                }
                if(res.length()>1){
                    res = res.substring(0, res.length()-1);
                    mapentry.getKey().getListeReponse().add(res);
                }

            }
            else if (mapentry.getKey().getTypeReponse().equals('c')){
                for (Node n: mapentry.getValue()){
                    ComboBox<String> cb = (ComboBox<String>) n;
                    try{
                        if (cb.getValue() != null){
                            res += Integer.toString(mapentry.getKey().getLesNomsValPossible().indexOf(cb.getValue())+1)+";";
                        }
                    }
                    catch(NullPointerException e){
                        ;
                    }
                }
                if(res.length()>1){
                    res = res.substring(0, res.length()-1);
                    mapentry.getKey().getListeReponse().add(res);
                }
            
            }
            if (res.equals("")){
                repond = false;
            }
            reponse.put(mapentry.getKey(), res);
            
        }
        if (repond){
            for (Map.Entry<Question, String> mapentry2 : reponse.entrySet()){
                RequeteSQL.saveReponseQuestion(mapentry2.getValue(), mapentry2.getKey(),this.appli.getConnexionMySQL(), this.vueSondeur);
                
            }
            RequeteSQL.sondeARepondu(this.appli.getConnexionMySQL(), this.vueSondeur, this.appli);
            this.vueSondeur.retirerSondeurDuPannel();
            this.vueSondeur.changerQuestionnaire(this.vueSondeur.getQuestionnaire());
            this.vueSondeur.miseAjourAffichage();

            if(this.vueSondeur.getPanelEnCours().size() == 0){
                this.appli.questionnaire_fini(this.vueSondeur.getQuestionnaire());
                this.vueSondeur.getBoutonActualiser().fire();
                this.vueSondeur.clearLabel();

            }
            else{
                this.vueSondeur.premierOnglet();
            }
        }
        else{
            this.vueSondeur.popUpValideToutRepondu().showAndWait();
        }
        

       
    }
}