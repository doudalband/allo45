public class Categorie {
    

    static char idCat;
    private String intituleCat;

    public Categorie(char idCat, String intituleCat){
        Categorie.idCat = Character.forDigit(Character.getNumericValue(idCat)+1, 10);
        this.intituleCat = intituleCat; 
    }

    public static char getIdCat() {
        return idCat;
    }

    public String getIntituleCat() {
        return intituleCat;
    }

    public static void setIdCat(char idCat) {
        Categorie.idCat = idCat;
    }

    public void setIntituleCat(String intituleCat) {
        this.intituleCat = intituleCat;
    }
}
