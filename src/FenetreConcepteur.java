import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;



public class FenetreConcepteur extends BorderPane {
    
    private Button ajouterQuestion;
    private Button creerQuestionnaire;
    private Button validerQuestion;
    private BorderPane centre;
    private VBox right;
    private ScrollPane centreQuestions;
    private GridPane creerQuestion;
    private HBox titre;
    private VBox sondages;        
    private Questionnaire questionnaireCourant;
    private ComboBox<String> choixTypeRep;
    private TextArea laQuestion;
    private SplitPane sp;
    private int maxVal;
    private char typeR;
    private TilePane lesReponses; 
    private BorderPane fenetreLesReponses;
    private List<TextField> listeRep;
    private Button supprimerRep;
    private Button ajouterRep; 
    private TextField tfNote;
    private Button boutonConfirm;
    private TextField tftelephone;
    private TextField tfville;
    private TextField tfcomplAdresse;
    private TextField tfadresse;
    private TextField tfcodePostal;
    private TextField tfemail;
    private TextField tfraisonSoc;
    private Button btnValiderClient;
    private ListeClients modeleClient;
    private List<String> nomClients;
    private ComboBox<String>  comboClient;
    private ComboBox<String>  comboPane;
    private TextField tfNomSond;
    private RadioButton triEntreprise;
    private RadioButton triDate;
    private ListePanels listePanels;
    private List<String> nomPanels;
    private GridPane lesQuestions;
    private List<VBox> lesQuestiosnModifiables;
    private Button modifierQuestion;
    private Label titreQuestionnaire;
    private MenuButton save_questionnaire;
    private ImageView plus;
    private Button creerQuestionnaire1;
    private Button boutonAnnulerCreaQuestion;
    private Button boutonAnnulerCreaQuest;


    public FenetreConcepteur(ListeClients modeleClient, Button btnValiderClient, Button btnConfirmerNouveauSondage, ListePanels listePanels, Button btvaliderQuestion,MenuButton save_questionnaire){
        super();
        this.plus = new ImageView("plus2.png");
        this.creerQuestionnaire1 = new Button("Créer un questionnaire",plus);
        this.save_questionnaire = save_questionnaire;
        this.typeR = ' ';
        this.boutonAnnulerCreaQuest = new Button("Annuler");
        this.boutonAnnulerCreaQuestion = new Button("Annuler");
        this.lesQuestions = new GridPane();
        this.lesQuestions.setVgap(20); 
        this.modeleClient = modeleClient;
        this.btnValiderClient = btnValiderClient;
        this.sondages = new VBox();
        this.nomClients = new ArrayList<>();
        this.nomPanels = new ArrayList<>();
        this.choixTypeRep = new ComboBox<String>();
        this.listeRep = new ArrayList<>();
        this.supprimerRep = new Button("Supprimer Réponse");
        this.ajouterRep = new Button("Ajouter réponse");
        this.ajouterRep.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-text-base-color: White;");
        this.supprimerRep.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-text-base-color: White;");
        this.lesReponses = new TilePane();
        this.lesReponses.setMaxSize(500, 40);
        this.lesReponses.setHgap(10); 
        this.lesReponses.setVgap(6); 
        this.tfNote = new TextField();
        this.lesQuestiosnModifiables = new ArrayList<>();
        this.tfNote.setStyle("-fx-control-inner-background : #edf5fa; -fx-font-family: Times New Roman; -fx-font-size : 120%; -fx-font-weight : bold; -fx-font-size : 10pt; -fx-background-radius : 15px 15px 15px 15px;");
        this.fenetreLesReponses = new BorderPane(boxChoixUnique());
        this.choixTypeRep = choixTypeRep(); //créer le comboBox avec les type de réponses possibles
        this.laQuestion = new TextArea("");
        this.ajouterQuestion = new Button("Ajouter Question");
        this.creerQuestionnaire = new Button("Créer un Questionnaire");
        this.validerQuestion = btvaliderQuestion;
        this.validerQuestion.setMinSize(150, 40);
        this.validerQuestion.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-font-size : 13pt; -fx-text-base-color: White;");
        this.centreQuestions = center(); //  à re-modifier
        this.right = right();
        this.titreQuestionnaire = new Label("Questionnaire");
        this.titre = titre();
        this.centre = new BorderPane();
        this.centre.setTop(this.titre);
        this.centre.setCenter(this.centreQuestions);
        this.centre.setPadding(new Insets(10));
        this.sp = new SplitPane();
        this.creerQuestion = fenetreAjouterQuestion();
        
        
        
        this.modifierQuestion = new Button("Modifier");
        this.modifierQuestion.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-font-size : 13pt; -fx-text-base-color: White;");
        this.centreQuestions.setStyle("-fx-background-color : #e2f0f8");
        
        this.boutonConfirm =  btnConfirmerNouveauSondage;

        this.setCenter(sp);
        this.setRight(this.right);
        this.sp.setOrientation(Orientation.HORIZONTAL);
        this.sp.getItems().addAll(this.centre, this.right);
        this.sp.setDividerPositions(0.831);
        this.sp.setStyle("-fx-background-color : #badfdd");
        //this.setCentreAjouterQuestion();
        
        
        this.supprimerRep.setOnAction(new ControleurSupprimerReponse(this));
        this.choixTypeRep.setOnAction(new ControleurChoixReponse(this));
        this.ajouterQuestion.setOnAction(new ControleurPageAddQuestion(this));
        this.boutonAnnulerCreaQuest.setOnAction((new ControleurAnnulerQuestionnaire(this)));
        this.boutonAnnulerCreaQuestion.setOnAction((new ControleurAnnulerCreationQuestion(this)));
        this.listePanels = listePanels;

        


        // this.setRight(asideRight());
        // this.setCenter(center());
    }

    public Questionnaire getQuestionnaireCourant() {
        return questionnaireCourant;
    }    

    public MenuButton getMenuButton(){
        return this.save_questionnaire;
    } 

    public List<VBox> getLesQuestionsModifiables(){
        return this.lesQuestiosnModifiables;
    }

    public ListePanels getListePanels(){
        return this.listePanels;
    }

    public void setQuestionnaireCourant(Questionnaire questionnaire){
        this.questionnaireCourant = questionnaire;
        for(MenuItem item : this.save_questionnaire.getItems()){
            item.setDisable(false);
        }
    }
    public TextArea getLaQuestion() {
        return laQuestion;
    }

    public TilePane getLesReponse(){
        return this.lesReponses;
    }
    public int getMaxVal() {
        return this.maxVal;
    }

    public char getTypeR() {
        return this.typeR;
    }

    public void setTypeR(char typeR) {
        this.typeR = typeR;
    }

    public ComboBox<String> getChoixTypeRep() {
        return choixTypeRep;
    }
    

    public void setFenetreLesReponsesUniq() throws Exception{
        if(this.listeRep.size() < 16){
            TextField tf = new TextField();
            tf.setStyle("-fx-control-inner-background : #edf5fa; -fx-font-family: SemiBold; -fx-font-size : 120%; -fx-background-radius : 15px 15px 15px 15px;");
            this.listeRep.add(tf);
            this.lesReponses.getChildren().add(tf);           
        }
        else{
            throw new Exception();
        }
        
    }

    public void supprimeReponse(){
        if(this.listeRep.size()>0 && this.lesReponses.getChildren().get(listeRep.size()-1) instanceof TextField){
            this.lesReponses.getChildren().remove(listeRep.size()-1);
            this.listeRep.remove(this.listeRep.size()-1);
        }
    }

    public void setFenetreLesReponses(BorderPane pane){
        this.fenetreLesReponses.setCenter(pane);
    }

    public List<TextField> getListeRep() {
        return listeRep;
    }

    public void validerReponse(){
        for(int i = 0; i<listeRep.size(); i++){
            TextField tf = (TextField) listeRep.get(i);
            String text = tf.getText();
            if(text.equals("")){
                this.lesReponses.getChildren().remove(i);
                this.listeRep.remove(i);
                i--;
            }
        }
    }
    
    public void setMaxVal(int maxVal) {
        this.maxVal = maxVal;
    }

    public HBox titre(){
        HBox hbTitre = new HBox(10);
        
        this.titreQuestionnaire.setFont(new Font("Arial", 35));
        this.titreQuestionnaire.setTextFill(Color.WHITE);
        this.titreQuestionnaire.setStyle("-fx-font-weight: bold;");
        hbTitre.getChildren().add(this.titreQuestionnaire);
        hbTitre.setStyle("-fx-background-color : #2498D9; -fx-background-radius : 20px 20px 0 0;");
        hbTitre.setPadding(new Insets(10));
        return hbTitre;
    }

    public ScrollPane center(){
        VBox vbbbb = new VBox(this.lesQuestions);
        ScrollPane scrollP = new ScrollPane();
        scrollP.setVbarPolicy(javafx.scene.control.ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollP.setFitToHeight(true);
        scrollP.setFitToWidth(true);
        scrollP.setContent(vbbbb);
        vbbbb.setAlignment(Pos.TOP_CENTER);
        vbbbb.setPadding(new Insets(15));
        vbbbb.setStyle("-fx-background-color : #e2f0f8 ;");
        this.ajouterQuestion.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-text-base-color: White; -fx-font-size : 15pt; -fx-font-family : \"Arial\";");

        return scrollP;
    }



    public VBox right(){


        VBox toulemid = new VBox();
        //

        HBox hbTitre = new HBox(10);
        Label titreH = new Label("Sujets / Thèmes");
        titreH.setFont(new Font("Arial", 20));
        titreH.setTextFill(Color.WHITE);
        titreH.setStyle("-fx-font-weight: bold");
        hbTitre.getChildren().add(titreH);
        hbTitre.setStyle("-fx-background-color : #2498D9; -fx-background-radius : 20px 20px 0 0;");
        hbTitre.setPadding(new Insets(10));


        VBox asideRight = new VBox();
        asideRight.setStyle("-fx-background-color : #e2f0f8 ;");
        asideRight.setMinSize(80, 80);
        asideRight.setPadding(new Insets(10));
        
        // Bouton pour ajouter un questionnaire
        ImageView imgPlusClient = new ImageView("plus2.png");
        imgPlusClient.setFitHeight(21);
        imgPlusClient.setFitWidth(21);
        imgPlusClient.setPreserveRatio(true);
        this.creerQuestionnaire.setGraphic(imgPlusClient);
        this.creerQuestionnaire.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-font-size : 11pt; -fx-text-base-color: White;");
        this.creerQuestionnaire.setOnAction(new ControleurCreerQuestionnaire(this));
        // Boite de tri des questionnaires réalisé(s)
        VBox trie = new VBox(5);
        trie.setPadding(new Insets(10));
        trie.setStyle("-fx-background-color : #b6d8eb");
        Label titre = new Label("Trier par :");
        titre.setStyle("-fx-font-weight: bold;  -fx-font-size : 150%;");
        HBox trierParEntreprise = new HBox();
        Label entreprise = new Label("• Entreprise ");
        entreprise.setStyle("-fx-font-size : 130%");
        HBox trierParDate = new HBox();
        Label date = new Label("• Date ");
        date.setStyle("-fx-font-size : 130%");
        ToggleGroup tg = new ToggleGroup();    
        this.triEntreprise = new RadioButton();
        this.triDate = new RadioButton();
        triEntreprise.setToggleGroup(tg);
        triDate.setToggleGroup(tg);
        triDate.setSelected(true);
        trierParEntreprise.getChildren().addAll(entreprise, triEntreprise);
        trierParDate.getChildren().addAll(date, triDate);
        trie.getChildren().addAll(titre, trierParEntreprise, trierParDate);
        
        triDate.setOnAction(new ControleurTrieDate(this));
        triEntreprise.setOnAction(new ControleurTriEntreprise(this));
       


                
        plus.setFitHeight(22);
        plus.setFitWidth(22);
        plus.setPreserveRatio(true);

        creerQuestionnaire1.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-font-size : 11pt; -fx-text-base-color: White;");
        creerQuestionnaire1.setOnAction(new ControleurCreerQuestionnaire(this));
        toulemid.getChildren().addAll(creerQuestionnaire1, trie);
        toulemid.setSpacing(10);

        //
        asideRight.getChildren().addAll(hbTitre, toulemid,sondages);
        asideRight.setSpacing(10);
        asideRight.setPadding(new Insets(10));



        
        return asideRight;

    }

    public RadioButton getTrieDate(){
        return this.triDate;
    }
    public RadioButton getTrieEntreprise(){
        return this.triEntreprise;
    }

    public void clearCouleur(){
        for(VBox vb : this.lesQuestiosnModifiables){
            vb.setStyle("-fx-background-color: transparent; -fx-cursor:hand;");
            for(Node node : vb.getChildren()){
                TextFlow textflow = (TextFlow) node;
                for(Node text : textflow.getChildren()){
                    Text texte = (Text) text;
                    texte.setFill(Color.BLACK);

                }
            }
        }
    }

    public void afficheQuestionnaire(Map<Questionnaire, Client> listeQuestionnaire){
        this.sondages.getChildren().clear();
        this.lesQuestiosnModifiables.clear();
        for(Questionnaire questionnaire : listeQuestionnaire.keySet()){
            VBox vb = new VBox();
            vb.setStyle("-fx-cursor:hand;");
            this.lesQuestiosnModifiables.add(vb);
            
            vb.setOnMouseClicked(new ControleurCliqueQuestionnaireDroite(this,questionnaire,vb));
            TextFlow flow = new TextFlow();
            Text raisonsocial = new Text(listeQuestionnaire.get(questionnaire).getRaisonSociale() + "\n");
            raisonsocial.setStyle("-fx-font-weight: bold");

            Text text = new Text(questionnaire.getIdQuestionnaire() + " - ");
            Text text2 = new Text(questionnaire.getNomQuestionnaire());
            


            text.setFont(Font.font("Arial",20));
            raisonsocial.setFont(Font.font("Arial",20));
            text2.setFont(Font.font("Arial",20));

            flow.getChildren().addAll(text, raisonsocial, text2);
            vb.getChildren().add(flow);
            vb.setPadding(new Insets(10));
            this.sondages.getChildren().addAll(vb);
        }
        
    }
    public void setCentreAjouterQuestion(){
        this.fenetreLesReponses.getChildren().clear();
        this.choixTypeRep.setValue("Réponse libre");
        this.centre.setCenter(fenetreAjouterQuestion());
    }

    public void setCentreModifierQuestion(Question question){
        this.centre.setCenter(fenetreModifierQuestion(question));
    }
    
    public void setCentreAjouterQuestionnaire(){
        
        this.centre.setCenter(this.ajouterQuestionnaire());
    }

    public void setCenterVoirQuestions(){
        this.centre.setCenter(this.centre);
    }

    public void setCentreAjouterClient(){
        
        this.centre.setCenter(this.ajouterClient());
    }

    public String getTfValMax(){
        return this.tfNote.getText();}


    public BorderPane ajouterQuestionnaire(){
        
        BorderPane bp = new BorderPane();
        bp.setStyle("-fx-background-color : #e2f0f8");
        HBox nameCompany = new HBox(20);
        HBox nameSubject = new HBox(20);
        HBox namePanel = new HBox(20);

        Label selectionner = new Label("Sélectionner le client");
        selectionner.setFont(Font.font("Arial",20));
        selectionner.setStyle("-fx-font-weight: bold");
        // La liste des entreprises déjà créer

        ObservableList<String> liste2 = FXCollections.observableList(this.nomClients); 
        
        this.comboClient = new ComboBox<>();
        this.comboClient.setItems(liste2);// atribution de la liste au ComboBox
        new AutoCompleteBox(this.comboClient);
        this.comboClient.setStyle("-fx-text-fill: -fx-text-inner-color; -fx-border-color : transparent;");

        Label labOu = new Label("OU");
        labOu.setFont(Font.font("Arial",20));
        labOu.setStyle("-fx-font-weight: bold");
        ImageView imgPlusClient = new ImageView("plus2.png");
        imgPlusClient.setFitHeight(17);
        imgPlusClient.setFitWidth(17);
        imgPlusClient.setPreserveRatio(true);

        Button boutonAjouterClient = new Button("Nouveau client",  imgPlusClient);
        boutonAjouterClient.setAlignment(Pos.CENTER);
        boutonAjouterClient.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-text-base-color: White; -fx-font-size : 13pt; -fx-font-family : \"Arial\";");
        boutonAjouterClient.setOnAction(new ControleurCreerClient(this));


        Label selectPan = new Label("Sélectionner le pannel");
        selectPan.setFont(Font.font("Arial",20));
        selectPan.setStyle("-fx-font-weight: bold");

        ObservableList<String> liste3 = FXCollections.observableList(this.nomPanels); 
        this.comboPane = new ComboBox<>();
        this.comboPane.setItems(liste3);// atribution de la liste au ComboBox
        new AutoCompleteBox(this.comboPane);
        this.comboPane.setStyle("-fx-text-fill: -fx-text-inner-color; -fx-border-color : transparent;");
        namePanel.getChildren().addAll(selectPan, this.comboPane);
        
        this.tfNomSond = new TextField();
        this.tfNomSond.setMinSize(220, 40);
        this.tfNomSond.setPromptText("Titre du sondage");
        this.tfNomSond.setStyle("-fx-font-size: 66pt;-fx-control-inner-background : #edf5fa; -fx-font-family: Times New Roman; -fx-font-size : 120%; -fx-font-weight : bold; -fx-font-size : 11pt; -fx-background-radius : 10px 10px 10px 10px;");
        

        this.boutonConfirm.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-text-base-color: White; -fx-font-size : 13pt; -fx-font-family : \"Arial\" ");
        nameCompany.getChildren().addAll(selectionner,this.comboClient);
        this.boutonConfirm.setAlignment(Pos.CENTER);
        //nameCompany.setStyle("-fx-background-color : red;");
        nameSubject.getChildren().addAll(this.tfNomSond);

        
        VBox tout = new VBox(10);
        nameCompany.setAlignment(Pos.CENTER);
        nameSubject.setAlignment(Pos.CENTER);
        namePanel.setAlignment(Pos.CENTER);
        VBox boxClient = new VBox(30);
        boxClient.setAlignment(Pos.CENTER); 
        boxClient.getChildren().addAll(nameCompany, labOu, boutonAjouterClient);
        VBox boxPanel = new VBox();
        boxPanel.getChildren().add(namePanel);
        boxPanel.setAlignment(Pos.CENTER); 
        VBox boxSondage = new VBox(30);
        boxSondage.setAlignment(Pos.CENTER); 
        HBox boxBouton = new HBox(20);
        this.boutonAnnulerCreaQuest.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-text-base-color: White; -fx-font-size : 13pt; -fx-font-family : \"Arial\" ");
        boxBouton.getChildren().addAll(this.boutonConfirm, this.boutonAnnulerCreaQuest);
        boxBouton.setAlignment(Pos.CENTER);
        boxSondage.getChildren().addAll(nameSubject, boxBouton);
        tout.getChildren().addAll(boxClient, boxPanel, boxSondage);
        //tout.setPadding(new Insets(10));
        tout.setAlignment(Pos.CENTER);
        tout.setSpacing(70);
        bp.setCenter(tout);



        return bp;
    }
    public String getComboTypeRep(){
        return this.choixTypeRep.getValue();
    }
    public String getComboClient(){
        return this.comboClient.getValue();
    }
    public String getComboPane(){
        return this.comboPane.getValue();
    }
    public String getNomSondage(){
        return this.tfNomSond.getText();}
        
    public void nomPanel() {
        this.nomPanels.clear();
        for(Panel panel : this.listePanels.getListePan()){
            this.nomPanels.add(panel.getNomPan());   
        }
    }

    public void nomClients() {
        this.nomClients.clear();
        for(Client client : this.modeleClient.getListeClient()){
            this.nomClients.add(client.getRaisonSociale());   
        }
    }


    public void miseAjourAffichage(){
        clearCouleur();
        if(this.getQuestionnaireCourant() != null){
            this.titreQuestionnaire.setText("Questionnaire - "+ this.getQuestionnaireCourant().getNomQuestionnaire());
            for(Node node: this.sondages.getChildren()){
                VBox unSondage = (VBox) node;
                for(Node textflownode : unSondage.getChildren()){
                    TextFlow textflow = (TextFlow) textflownode;
                    Text text = (Text) textflow.getChildren().get(2);
                    Text id = (Text) textflow.getChildren().get(0);
                    int idquestionnaire = Integer.parseInt(id.getText().replace(" ","").replace("-",""));
                    if(text.getText().equals(this.questionnaireCourant.getNomQuestionnaire()) && idquestionnaire == this.questionnaireCourant.getIdQuestionnaire()){
                        unSondage.setStyle("-fx-background-color:#2498D9;-fx-cursor:hand;-fx-font-color : #FFFFFF");
                        for(Node element_vbox : textflow.getChildren()){
                            Text questionnaire = (Text) element_vbox;
                            questionnaire.setFill(Color.WHITE);
                        }
                    }
                }
            }
        }
        else{
            this.titreQuestionnaire.setText("Questionnaire");
        }
        this.nomClients();
        this.nomPanel();
        miseAjourLesQuestions();

        
    }
    
    
    private void miseAjourLesQuestions() {
        this.lesQuestions.getChildren().clear();
        VBox vb = (VBox) this.centreQuestions.getContent();
        vb.getChildren().clear();
        Questionnaire questionnaire = this.getQuestionnaireCourant();
        int cpt = 0;
        this.lesQuestions.setAlignment(Pos.TOP_CENTER); 
        List<Character> listeTypeRep = new ArrayList<>(Arrays.asList('l','m', 'c', 'u','n'));
        List<String> listeReponse  = new ArrayList<>(Arrays.asList("Réponse libre", "Choix multiples", "Classement", "Réponse unique", "Notes"));
        if(questionnaire != null){ 
            
            for(Question question : questionnaire.getListeQuestions()){
                cpt++;
                VBox numeroQuestion = new VBox();
                Label numQuest = new Label(cpt+"");
                numQuest.setStyle("-fx-padding : 8 12 8 12;");     
                numQuest.setPadding(new Insets(7)); 
                numQuest.setFont(Font.font("Arial",FontWeight.BOLD,28));
                numeroQuestion.getChildren().add(numQuest);// numéro de la question
                numeroQuestion.setStyle("-fx-background-color : #25bff7; -fx-font-family: Times New Roman; -fx-font-size : 100%; -fx-font-weight : bold; -fx-font-size : 11pt; -fx-background-radius : 25px 0px 0px 25px;");
                Label texteQuestion = new Label(question.getTexteQ()); // La question 
                texteQuestion.setStyle("-fx-padding : 8 12 8 12;");
                VBox boxTexteQUestion = new VBox();
                boxTexteQUestion.getChildren().add(texteQuestion);
                boxTexteQUestion.setStyle("-fx-background-color : #97d9fc; -fx-font-family: Times New Roman; -fx-font-size : 120%; -fx-font-weight : bold; -fx-font-size : 11pt;");
                texteQuestion.setFont(Font.font("Arial",FontWeight.BOLD,28));
                VBox boxRep = new VBox();
                for(int indiceRep = 0; indiceRep<listeReponse.size(); indiceRep++){
                    if(question.getTypeReponse().equals(listeTypeRep.get(indiceRep))){
                        Label typeRep = new Label("• "+listeReponse.get(indiceRep));
                        typeRep.setStyle("-fx-padding : 8 12 8 12;");
                        boxRep.getChildren().add(typeRep);
                        boxRep.setStyle("-fx-background-color : #97d9fc; -fx-font-family: Times New Roman; -fx-font-size : 120%; -fx-font-weight : bold; -fx-font-size : 11pt;");
                        typeRep.setFont(Font.font("Arial",FontWeight.BOLD,28));
                    }
                }
                VBox boxOnglet = new VBox();
                boxOnglet.setStyle("-fx-background-color : #2498D9; -fx-font-family: Times New Roman; -fx-font-size : 120%; -fx-font-weight : bold; -fx-font-size : 11pt; -fx-background-radius : 0px 25px 25px 0px;");
                MenuButton menuReponse = new MenuButton();
                menuReponse.setStyle("-fx-background-color: transparent;");
                ImageView imgOnglet = new ImageView("onglet4.png");
                imgOnglet.setFitHeight(50);
                imgOnglet.setFitWidth(50);
                imgOnglet.setPreserveRatio(true);                    
                menuReponse.setGraphic(imgOnglet);
                MenuItem modifier = new MenuItem("Modifier");
                MenuItem supprimer = new MenuItem("Supprimer");
                boxOnglet.getChildren().add(menuReponse);
                boxOnglet.setAlignment(Pos.CENTER);
                menuReponse.getItems().addAll(modifier,supprimer);
                modifier.setOnAction(new ControleurAfficheModifierQuestion(this,question));
                supprimer.setOnAction(new ControleurSupprimerQuestion(this, question));
            
                this.lesQuestions.add(numeroQuestion, 0, cpt);
                this.lesQuestions.add(boxTexteQUestion, 1, cpt);
                this.lesQuestions.add(boxRep, 2, cpt);
                this.lesQuestions.add(boxOnglet, 3, cpt);
                
            }

            this.lesQuestions.setAlignment(Pos.TOP_CENTER);
            vb.setSpacing(30);

            vb.getChildren().addAll(this.lesQuestions,this.ajouterQuestion);}

        else{
            this.lesQuestions.getChildren().add(this.creerQuestionnaire);
        }
    }



    public VBox ajouterClient(){

        List<String> listeNomChamps = new ArrayList<>(Arrays.asList("Raison Social", "Adresse", "Complément d'adresse", "Code postal", "Ville", "Telephone", "Email"));

        this.tfraisonSoc = new TextField();
        this.tfraisonSoc.setMinSize(170, 40);


        this.tfadresse = new TextField();
        this.tfadresse.setMinSize(170, 40);
        this.tfadresse.setPromptText("Adresse");

        this.tfcomplAdresse = new TextField();
        this.tfcomplAdresse.setMinSize(170, 40);
        this.tfcomplAdresse.setPromptText("Complément d'adresse");

        this.tfcodePostal = new TextField();
        this.tfcodePostal.setMinSize(170, 40);
        this.tfcodePostal.setPromptText("Code postal");

        this.tfville = new TextField();
        this.tfville.setMinSize(170, 40);
        this.tfville.setPromptText("Ville");

        this.tftelephone = new TextField();
        this.tftelephone.setMinSize(170, 40);
        this.tftelephone.setPromptText("Telephone");
        
        this.tfemail = new TextField();
        this.tfemail.setMinSize(170, 40);
        this.tfemail.setPromptText("Email");

    
        VBox vb = new VBox();
        vb.setStyle("-fx-background-color : #e2f0f8");
        vb.setAlignment(Pos.CENTER);

        Label titreC = new Label("AJOUTER UN CLIENT");
        titreC.setFont(Font.font("Arial",FontWeight.BOLD,32));
        titreC.setPadding(new Insets(10, 0 , 0, 0));


        TilePane millieu = new TilePane();
        millieu.setAlignment(Pos.CENTER);
        millieu.setHgap(80);
        millieu.setVgap(40);
        millieu.setPadding(new Insets(40));
        List<TextField> l1 = Arrays.asList(this.tfraisonSoc,this.tfadresse,this.tfcomplAdresse,this.tfcodePostal,
            this.tfville,this.tftelephone,this.tfemail);

            // Style et positionnement
            for(int i=0; i<l1.size(); i++){
                l1.get(i).setAlignment(Pos.CENTER);
                l1.get(i).setMinSize(170, 40);
                l1.get(i).setPromptText(listeNomChamps.get(i));
                l1.get(i).getStyleClass().add("textFieldClient");
                l1.get(i).setStyle("-fx-control-inner-background : #edf5fa; -fx-font-family: Times New Roman; -fx-font-size : 120%; -fx-font-weight : bold; -fx-background-radius : 25px 25px 25px 25px;");
                VBox v1 = new VBox();
                v1.getChildren().add(l1.get(i));
                v1.setSpacing(10);
                v1.setMaxWidth(1000);
                millieu.getChildren().addAll(v1);
        }
        HBox hb  = new HBox();
        Button btnAnnuler = new Button("Annuler");
        hb.setSpacing(50);
        btnAnnuler.setOnAction(new ControleurAnnulerCreationClient(this));
        hb.setAlignment(Pos.CENTER);

        btnAnnuler.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-text-base-color: White; -fx-font-size : 13pt;"); 
        btnAnnuler.setPrefWidth(150);

        this.btnValiderClient.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-text-base-color: White; -fx-font-size : 13pt;");
        this.btnValiderClient.setPrefWidth(150);

        hb.getChildren().addAll(this.btnValiderClient, btnAnnuler);
        vb.getChildren().addAll(titreC,millieu,hb);
        return vb;
    }



    public String getRaisonSocial(){return this.tfraisonSoc.getText();}
    public String getAdresse(){return this.tfadresse.getText();}
    public String getComplAdresse(){return this.tfcomplAdresse.getText();}
    public String getCodePostal(){return this.tfcodePostal.getText();}
    public String getVille(){return this.tfville.getText();}
    public String getEmail(){return this.tfemail.getText();}
    public String getTelephone(){return this.tftelephone.getText();}


    public GridPane fenetreAjouterQuestion(){
        GridPane fenetreAjouterQuestions = new GridPane();
        this.lesReponses.getChildren().clear();
        this.laQuestion.clear();
        fenetreAjouterQuestions.setVgap(30);
        fenetreAjouterQuestions.setStyle("-fx-background-color : #e2f0f8");
        HBox BoxQuestion = new HBox(20);
        Label question = new Label("Question");
        question.setStyle("-fx-font-weight : bold; -fx-font-size : 180%;");
        this.laQuestion.setStyle("-fx-font-size : 125%; -fx-background-color : #d5e8f2; -fx-control-inner-background: #f0f8fc; -fx-prompt-text-fill: #028ed9;");
        this.laQuestion.setPrefSize(300, 45);
        this.laQuestion.setWrapText(true);
        BoxQuestion.getChildren().addAll(question, this.laQuestion);
        HBox BoxReponse = new HBox(20);
        Label reponse = new Label("Type de réponse");
        reponse.setStyle("-fx-font-weight : bold; -fx-font-size : 180%;");
        BoxReponse.getChildren().addAll(reponse, this.choixTypeRep);
        fenetreAjouterQuestions.add(BoxQuestion, 0,0);
        fenetreAjouterQuestions.add(BoxReponse, 0,1);
        fenetreAjouterQuestions.add(this.fenetreLesReponses, 0, 2);
        this.validerQuestion.setAlignment(Pos.CENTER); // a modifier
        HBox boxBoutonBottom = new HBox();
        boxBoutonBottom.setSpacing(15);
        this.boutonAnnulerCreaQuestion.setMinSize(100, 40);
        this.boutonAnnulerCreaQuestion.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-font-size : 13pt; -fx-text-base-color: White;");
        boxBoutonBottom.getChildren().addAll(this.validerQuestion, this.boutonAnnulerCreaQuestion);
        fenetreAjouterQuestions.add(boxBoutonBottom, 0, 3); 


        fenetreAjouterQuestions.setAlignment(Pos.CENTER);
        return fenetreAjouterQuestions;
    }

    public Button getBtnValiderQuestion(){
        return this.validerQuestion;
    }


    public GridPane fenetreModifierQuestion(Question question){
        this.lesReponses.getChildren().clear();
        GridPane fenetreAjouterQuestions = new GridPane();
        fenetreAjouterQuestions.setVgap(30);
        fenetreAjouterQuestions.setStyle("-fx-background-color : #e2f0f8");
        HBox BoxQuestion = new HBox(20);
        Label labelQuestion = new Label("Question");
        labelQuestion.setStyle("-fx-font-weight : bold; -fx-font-size : 180%;");
        this.laQuestion.setStyle("-fx-font-size : 125%; -fx-background-color : #d5e8f2; -fx-control-inner-background: #f0f8fc; -fx-prompt-text-fill: #028ed9;");
        this.laQuestion.setPrefSize(300, 45);
        this.laQuestion.setWrapText(true);
        this.laQuestion.setText(question.getTexteQ());
        BoxQuestion.getChildren().addAll(labelQuestion, this.laQuestion);
        HBox BoxReponse = new HBox(20);
        Label reponse = new Label("Type de réponse");
        reponse.setStyle("-fx-font-weight : bold; -fx-font-size : 180%;");
        switch (question.getTypeReponse()){
            case 'u' : 
                this.choixTypeRep.setValue("Réponse unique");
                break;
            case 'm' : 
                this.choixTypeRep.setValue("Choix multiples");
                break;
            case 'c' : 
                this.choixTypeRep.setValue("Classement");
                break;
            case 'n' : 
                this.choixTypeRep.setValue("Note");
                break;
            case 'l' : 
                this.choixTypeRep.setValue("Réponse libre");
                break;
        }
        int nbRep = question.getListeValPossible().size();
        for(int i = 0; i<nbRep; i++){
            TextField tf = new TextField(question.getListeValPossible().get(i).getValeur());
            tf.setStyle("-fx-control-inner-background : #edf5fa; -fx-font-family: SemiBold; -fx-font-size : 120%; -fx-background-radius : 15px 15px 15px 15px;");
            lesReponses.getChildren().add(tf);
            this.getListeRep().add(tf);
        }
        BoxReponse.getChildren().addAll(reponse, this.choixTypeRep);
        fenetreAjouterQuestions.add(BoxQuestion, 0,0);
        fenetreAjouterQuestions.add(BoxReponse, 0,1);
        fenetreAjouterQuestions.add(this.fenetreLesReponses, 0, 2);
        this.modifierQuestion.setAlignment(Pos.CENTER); // a modifier
        this.modifierQuestion.setOnAction(new ControleurModifierQuestion(this, question));
        
        fenetreAjouterQuestions.add(this.modifierQuestion, 0, 3); 


        fenetreAjouterQuestions.setAlignment(Pos.CENTER);
        return fenetreAjouterQuestions;
    }


    public void afficheLesQuestions(){
        this.centre.setCenter(this.centreQuestions);
    }

    public void afficherAjouterQuestion(){
        this.centre.setCenter(this.creerQuestion);
    }


    public ComboBox<String> choixTypeRep(){
        List<String> listeChoixRep = new ArrayList<>(Arrays.asList("Réponse unique", "Choix multiples", "Classement", "Note", "Réponse libre")); 
        ObservableList<String> obsReponse = FXCollections.observableList(listeChoixRep); 
        this.choixTypeRep.setItems(obsReponse);
        //this.choixTypeRep.setValue(obsReponse.get(1));
        this.setTypeR('l');
        return choixTypeRep;
    }

    public BorderPane boxChoixUnique(){
        BorderPane configTypeReponse = new BorderPane();
        this.lesReponses.setAlignment(Pos.CENTER);
        this.lesReponses.getChildren().addAll(ajouterRep);
        this.ajouterRep.setOnAction(new ControleurAjouterReponse(this));
        GridPane gridboutons = new GridPane();
        VBox boxboutons = new VBox(30);
        boxboutons.getChildren().addAll(this.ajouterRep, this.supprimerRep); //, this.modifierRep,this.validerRep);
        gridboutons.add(boxboutons, 0, 0);
        boxboutons.setAlignment(Pos.CENTER);
        configTypeReponse.setLeft(boxboutons);
        configTypeReponse.setStyle("-fx-background-color : #b6d8eb");
        this.lesReponses.setAlignment(Pos.TOP_CENTER);
        configTypeReponse.setCenter(this.lesReponses);
        configTypeReponse.setPadding(new Insets(10));
        return configTypeReponse;
    }


    public Alert popMaxQuestion(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, " La limite de question atteintes \n La limite est de 16 questions");
        alert.setTitle("Limite Question");
        return alert;
    }

    public Alert popUpPasDeTypeRep(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Vous devez sélectionner un type de réponse");
        alert.setTitle("Type de réponse");
        return alert;
    }

    public Alert popQuestionnaireSave(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Le questionnaire : " + questionnaireCourant.getNomQuestionnaire() + " à bien été enregistré");
        alert.setTitle("Save Questionnaire");
        return alert;
    }

    public Alert popQuestionnaireDelete(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION," Voulez-vous vraiment supprimer le questionnaire ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }

    public Alert popQuestionnaireFini(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Le questionnaire : " + questionnaireCourant.getNomQuestionnaire() + " à été classé comme fini");
        alert.setTitle("Questionnaire Fini");
        return alert;
    }

    public Alert popQuestionVide(){
        Alert alert = new Alert(Alert.AlertType.WARNING, "Entrez une question pour valider");
        alert.setTitle("ERREUR");
        return alert;
    }
    public Alert popUpNbValide(){
        Alert alert = new Alert(Alert.AlertType.WARNING, "Entrez un nombre valide");
        alert.setTitle("ERREUR");
        return alert;
    }
    public Alert popUp2rep(){
        Alert alert = new Alert(Alert.AlertType.WARNING, "Entrez au moins deux réponses \n ou changer le nombre max");
        alert.setTitle("ERREUR");
        return alert;
    }
    public Alert popUpSuprQuestionnaire(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Voulez-vous vraiment supprimer ce Questionnaire ?", ButtonType.NO, ButtonType.YES );
        alert.setTitle("Supprimer Questionnaire");
        return alert;
    }

    public Alert popUpValideQuestionnaire(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Voulez-vous vraiment valider le Questionnaire ?", ButtonType.NO, ButtonType.YES );
        alert.setTitle("Valider Questionnaire");
        return alert;
    }
    public BorderPane boxChoixMultiple(String msg){
        BorderPane configTypeReponse = new BorderPane();
        this.lesReponses.setAlignment(Pos.CENTER);
        this.lesReponses.getChildren().addAll(ajouterRep);
        this.ajouterRep.setOnAction(new ControleurAjouterReponse(this));
        GridPane gridboutons = new GridPane();
        VBox boxboutons = new VBox(20);
        this.tfNote.setPromptText(msg);
        boxboutons.getChildren().addAll(this.ajouterRep, this.supprimerRep, this.tfNote); //, this.modifierRep, this.validerRep);
        gridboutons.add(boxboutons, 0, 0);
        boxboutons.setAlignment(Pos.CENTER);
        configTypeReponse.setLeft(boxboutons);
        configTypeReponse.setStyle("-fx-background-color : #b6d8eb");
        this.lesReponses.setAlignment(Pos.TOP_CENTER);
        configTypeReponse.setCenter(this.lesReponses);
        configTypeReponse.setPadding(new Insets(10));
        return configTypeReponse;
        
    }

    public BorderPane boxChoixClassement(String msg){
        return boxChoixMultiple(msg);
    }

    public BorderPane boxChoixNote() {
        BorderPane configTypeReponse = new BorderPane();
        this.tfNote.setPromptText("Note Maximale");
        this.tfNote.setAlignment(Pos.CENTER);
        VBox vb = new VBox(20);
        vb.getChildren().addAll(this.tfNote);
        vb.setAlignment(Pos.CENTER);
        configTypeReponse.setStyle("-fx-background-color : #b6d8eb");
        configTypeReponse.setPadding(new Insets(10));
        configTypeReponse.setCenter(vb);
        configTypeReponse.setMaxWidth(200);
        return configTypeReponse;
    }

    public void boxChoixLibre() {
        this.fenetreLesReponses.getChildren().clear();
    }

    public void trieQuestionnaireParDate() {
        
        List<Node> liste = (List<Node>) this.sondages.getChildren();
        List<Node> res = new ArrayList<>(liste);
        Collections.sort(res, new TrierQuestionnaireDate());
        this.sondages.getChildren().clear();
        for(Node n : res){
            this.sondages.getChildren().add(n);
        }
    }

    public void trieQuestionnaireParEntreprise() {
        List<Node> liste = (List<Node>) this.sondages.getChildren();
        List<Node> res = new ArrayList<>(liste);
        Collections.sort(res, new TrierQuestionnaireEntreprise());
        this.sondages.getChildren().clear();
        for(Node n : res){
            this.sondages.getChildren().add(n);
        }
    }
    public void valideQuestion(){
        Question q  = new Question(this.getQuestionnaireCourant().getListeQuestions().size()+1, this.getLaQuestion().getText(), this.getMaxVal(), this.getTypeR());

        for(TextField tf : this.getListeRep()){
            ValPossible val =  new ValPossible(q.getListeValPossible().size()+1, tf.getText());
            q.ajouterValPossible(val);
        }
        this.getQuestionnaireCourant().ajouteQuestion(q);
        this.miseAjourAffichage();
        this.afficheLesQuestions();
        this.getListeRep().clear();
    }

    public void valideChoixLibre(){
        validerReponse();
        boolean bon = true;   
        if (this.getLaQuestion().getText().isEmpty()){
            this.popQuestionVide().showAndWait();
            bon = false;
        }
        if (bon){
            this.valideQuestion();
        }
    }

    public void valideChoixUnique(){
        boolean bon = true;   
        validerReponse();
        if (2 > this.getListeRep().size()){
            this.popUp2rep().showAndWait();
            bon = false;
        }
        if (this.getLaQuestion().getText().isEmpty()){
            this.popQuestionVide().showAndWait();
            bon = false;
        }
        if (bon){
            this.valideQuestion();
        }
    }

    public void valideNotes(){
        boolean bon = true;   
        validerReponse();
        if (this.getTfValMax().isEmpty()){
            this.popUpNbValide().showAndWait();
            bon = false;
        }
        else{
            try{
                int val = Integer.parseInt(this.getTfValMax());
                if(val<1){
                    this.popUpNbValide().showAndWait();
                    bon = false;
                }
                else{this.setMaxVal(val);
                }
            }
            catch(NumberFormatException e){this.popUpNbValide().showAndWait();
            bon = false;}    
        }
        if (this.getLaQuestion().getText().isEmpty()){
            this.popQuestionVide().showAndWait();
            bon = false;
        }
        if (bon){
            this.valideQuestion();
        }
    }

    public void valideChoixMultiples(){
        boolean bon = true;
        validerReponse();
        if (this.getTfValMax().isEmpty()){
            this.popUpNbValide().showAndWait();
            bon = false;
        }
        else{
            try{
                int val = Integer.parseInt(this.getTfValMax());
                this.setMaxVal(val);
            }
            catch(NumberFormatException e){this.popUpNbValide().showAndWait();
            bon = false;}    
        }
        if (!(2<= this.getListeRep().size() && this.getMaxVal() <= this.getListeRep().size())){
            this.popUp2rep().showAndWait();
            bon = false;
        }
        if (this.getLaQuestion().getText().isEmpty()){
            this.popQuestionVide().showAndWait();
            bon = false;
        }
        if (bon){
            this.valideQuestion();
        }
    }

    public void valideClassement(){
        boolean bon = true;
        if (this.getTfValMax().isEmpty()){
            this.popUpNbValide().showAndWait();
            bon = false;
        }
        else{
            try{
                int val = Integer.parseInt(this.getTfValMax());
                this.setMaxVal(val);
            }
            catch(NumberFormatException e){this.popUpNbValide().showAndWait();
            bon = false;}    
        }
        validerReponse();
        if (!(2<= this.getListeRep().size() && this.getMaxVal() <= this.getListeRep().size())){
            this.popUp2rep().showAndWait();
            bon = false;
        }
        if (this.getLaQuestion().getText().isEmpty()){
            this.popQuestionVide().showAndWait();
            bon = false;
        }
        if (bon){
            this.valideQuestion();
        }
    }

    public void resetQuestionnaireCourant() {
        this.questionnaireCourant = null;
        for(MenuItem content : this.getMenuButton().getItems()){
            content.setDisable(true);
        }
    }
}
