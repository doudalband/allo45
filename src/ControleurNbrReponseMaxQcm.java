import java.util.List;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;

public class ControleurNbrReponseMaxQcm implements ChangeListener<Boolean>{
    private int activeCount;
    private int maxCount;
    private List<CheckBox> checkBoxes;

    public ControleurNbrReponseMaxQcm(Question q, List<CheckBox> checkBoxes){
        this.maxCount = q.getMaxVal();
        this.activeCount = 0;
        this.checkBoxes = checkBoxes;
    }
    @Override
    public void changed(ObservableValue<? extends Boolean> o, Boolean oldValue, Boolean newValue) {
        if (newValue) {
            this.activeCount++;
            if (this.activeCount == this.maxCount) {
                // disable unselected CheckBoxes
                for (CheckBox cb : this.checkBoxes) {
                    if (!cb.isSelected()) {
                        cb.setDisable(true);
                    }
                }
            }
        } else {
            if (this.activeCount == this.maxCount) {
                // reenable CheckBoxes
                for (CheckBox cb : this.checkBoxes) {
                    cb.setDisable(false);
                }
            }
            this.activeCount--;
        }
    }




    
}
