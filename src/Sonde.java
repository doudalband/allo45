import java.sql.Date;

public class Sonde {
    

    private int numSond; 
    private String nomSond;
    private String prenomSond;
    private Date dateNaisSond;
    private String telephoneSond;
    private Caracteristique caracteristique;
    
    
    public Sonde(int numSond, String nomSond, String prenomSond, Date dateNaisSond, String telephoneSond, Caracteristique caracteristique){
        this.numSond = numSond;
        this.nomSond  = nomSond;
        this.prenomSond = prenomSond; 
        this.dateNaisSond = dateNaisSond;
        this.telephoneSond = telephoneSond;
        this.caracteristique = caracteristique;
    }

    public Date getDateNaisSond() {
        return dateNaisSond;
    }
    
    public String getNomSond() {
        return nomSond;
    }
    
    public int getNumSond() {
        return numSond;
    }

    public String getPrenomSond() {
        return prenomSond;
    }

    public String getTelephoneSond() {
        return telephoneSond;
    }

    public Caracteristique getCaracteristique(){
        return this.caracteristique;
    }

    // Les Setters
    public void setDateNaisSond(Date dateNaisSond) {
        this.dateNaisSond = dateNaisSond;
    }

    public void setNomSond(String nomSond) {
        this.nomSond = nomSond;
    }

    public void setNumSond(int numSond) {
        this.numSond = numSond;
    }

    public void setPrenomSond(String prenomSond) {
        this.prenomSond = prenomSond;
    }
    
    public void setTelephoneSond(String telephoneSond) {
        this.telephoneSond = telephoneSond;
    }

    public void setCaracteristique(Caracteristique c){
        this.caracteristique = c;
    }

    @Override
    public String toString(){
        return this.numSond + ", " + this.nomSond + ", " + this.prenomSond + ", ddn : " + this.dateNaisSond +  ", tel : " + this.telephoneSond;
    }

    
}
