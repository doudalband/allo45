import javafx.event.ActionEvent;
import javafx.event.EventHandler;



public class ControleurSondeSuivant implements EventHandler<ActionEvent>{

    private FenetreSondeur vueSond;



    public ControleurSondeSuivant(FenetreSondeur vueSond) {
        this.vueSond = vueSond;
    }


    @Override
    public void handle(ActionEvent actionEvent){
        if(this.vueSond.getQuestionnaire() != null)
            this.vueSond.sondeSuiv();
            
    }















}