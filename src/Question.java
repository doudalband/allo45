
import java.util.ArrayList;
import java.util.List;
public class Question {

    public int idQ;
    private String texteQ;
    private Character typeReponse;
    private int maxVal;
    private List<ValPossible> listeValPossible;
    private List<String> reponses;

    public Question(int idQ, String texteQ, int maxVal, Character typeReponse){
        this.idQ = idQ;
        this.texteQ = texteQ;
        this.maxVal = maxVal;
        this.typeReponse = typeReponse;
        this.listeValPossible = new ArrayList<>();
        this.reponses = new ArrayList<>();
    }

    //Les Getters
    public int getIdQ() {
        return idQ;
    }

    public int getMaxVal() {
        return maxVal;
    }

    public String getTexteQ() {
        return texteQ;
    }

    public Character getTypeReponse() {
        return typeReponse;
    }

    //Les Setters
    public void setIdQ(int idQ) {
        this.idQ = idQ;
    }
    
    public void setMaxVal(int maxVal) {
        this.maxVal = maxVal;
    }

    public void setTexteQ(String texteQ) {
        this.texteQ = texteQ;
    }

    public void setTypeReponse(Character typeReponse) {
        this.typeReponse = typeReponse;
    }


    public void ajouterValPossible(ValPossible val){
        this.listeValPossible.add(val);
    }
    
    @Override
    public String toString(){
        return this.idQ + ", " + this.texteQ + ", " +  this.typeReponse;
    }
    public List<ValPossible> getListeValPossible() {
        return this.listeValPossible;
    }

    public List<String> getLesNomsValPossible() {
        List<String> l = new ArrayList<>();
        for (ValPossible v: this.listeValPossible){
            l.add(v.getValeur());
        }
        return l;
    }

    public List<String> getListeReponse() {
        return this.reponses;
    }
}
