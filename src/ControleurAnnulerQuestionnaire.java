import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurAnnulerQuestionnaire implements EventHandler<ActionEvent> {

    private FenetreConcepteur fenetreConcepteur;

    public ControleurAnnulerQuestionnaire(FenetreConcepteur fenetreConcepteur){
        this.fenetreConcepteur = fenetreConcepteur;
    }

    @Override
    public void handle(ActionEvent event){
        this.fenetreConcepteur.afficheLesQuestions();
    }
}
