import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ListeClients{
    
    
    private Client clientCourant;
    private List<Client> listeClient;
    

    public ListeClients(){
        this.listeClient = new ArrayList<>();
        this.clientCourant = null;
    }


    public List<Client> getListeClient(){
        return this.listeClient;
    }

    public boolean clientSuiv(Client c) throws NoSuchElementException{
        if(this.listeClient.size() == 0){throw new NoSuchElementException();}
        int indiceClientCourant = this.listeClient.indexOf(c);
        if(indiceClientCourant != -1 && indiceClientCourant+1 <= this.listeClient.size()-1){
            setClientCourant(this.listeClient.get(indiceClientCourant+1));
            return true;
        }
        else{
            return false;
        }
    }

    public boolean clientPrec(Client c) throws NoSuchElementException{
        if(this.listeClient.size() == 0){throw new NoSuchElementException();}
        int indiceClientCourant = this.listeClient.indexOf(c);
        if(indiceClientCourant != -1 && indiceClientCourant-1 >= 0){
            setClientCourant(this.listeClient.get(indiceClientCourant-1));
            return true;
        }
        else{
            return false;
        }
    }

    public void setClientCourant(Client clientCourant) {
        this.clientCourant = clientCourant;
    }


    public void ajouteClient(Client client){
        this.listeClient.add(client);
    }


    public void supprimerClient(Client client){
        this.listeClient.remove(client);
    }

    public void clearClient(){
        this.listeClient.clear();
    }
    public Client getClientCourant() {
            return clientCourant;
        }

    @Override
    public String toString(){
        return this.listeClient.toString();
    }
}
