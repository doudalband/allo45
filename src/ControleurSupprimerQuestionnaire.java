import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;

public class ControleurSupprimerQuestionnaire implements EventHandler<ActionEvent> {

    private AppliPlusieursFenetres appli;

    private FenetreConcepteur fenetreConcepteur;

    private ListeClients listeClient;

    public ControleurSupprimerQuestionnaire(AppliPlusieursFenetres appliPlusieursFenetres,
            FenetreConcepteur fenetreConcepteur, ListeClients listeClient) {

                this.appli = appliPlusieursFenetres;
                this.fenetreConcepteur = fenetreConcepteur;
                this.listeClient = listeClient;
    }

    @Override
    public void handle(ActionEvent event) {
        Optional<ButtonType> reponse = this.fenetreConcepteur.popQuestionnaireDelete().showAndWait();
        if(reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
            RequeteSQL.supprimerQuestionnaire(this.appli,this.fenetreConcepteur,this.listeClient);
        }

    }
}
