import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

public class ControleurRechercherQuestionnaire implements EventHandler<KeyEvent>{
    
    private AppliPlusieursFenetres appli;
    private FenetreSondeur fenetreSondeur;

    public ControleurRechercherQuestionnaire(AppliPlusieursFenetres appli, FenetreSondeur fenetreSondeur){
        this.fenetreSondeur = fenetreSondeur;
        this.appli = appli;
    }

    @Override
    public void handle(KeyEvent event){
        
        this.fenetreSondeur.mettreLesSondageRecherche(this.appli.chargerSondage());
        


        
    }
}
