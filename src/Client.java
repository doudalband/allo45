import java.util.ArrayList;
import java.util.List;

public class Client {
    
    private int numClient;
    private String raisonSociale;
    private String adresse1;
    private String adresse2;
    private String codePostal;
    private String ville;
    private String telephone;
    private String email;
    private List<Questionnaire> listeQuestionnaire;

    public Client(int numClient, String raisonSociale, String adresse1, String adresse2, String codePostal, String ville, String telephone, String email){
        this.numClient = numClient;
        this.raisonSociale = raisonSociale;
        this.adresse1 = adresse1;
        this.adresse2 = adresse2;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.email = email;
        this.listeQuestionnaire = new ArrayList<>();
    }

    // Les Getters
    public String getAdresse1() {
        return adresse1;
    }

    public List<Questionnaire> getListeQuestionnaire() {
        return listeQuestionnaire;
    }

    public String getAdresse2() {
        return adresse2;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public String getEmail() {
        return email;
    }

    public int getNumClient() {
        return numClient;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getVille() {
        return ville;
    }


    // Les Setters
    public void setAdresse1(String adresse1) {
        this.adresse1 = adresse1;
    }

    public void setAdresse2(String adresse2) {
        this.adresse2 = adresse2;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNumClient(int numClient) {
        this.numClient = numClient;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    public void setVille(String ville) {
        this.ville = ville;
    }

    public void ajouteQuestionnaire(Questionnaire questionnaire){
        this.listeQuestionnaire.add(questionnaire);
    }

}
