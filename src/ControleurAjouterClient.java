import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurAjouterClient implements EventHandler<ActionEvent> {

    private AppliPlusieursFenetres appli;

    private FenetreConcepteur fenetreConcepteur;

    public ControleurAjouterClient(AppliPlusieursFenetres appliPlusieursFenetres, ListeClients listeClient,
            FenetreConcepteur fenetreConcepteur) {
                this.appli = appliPlusieursFenetres;
                this.fenetreConcepteur = fenetreConcepteur;

    }

    @Override
    public void handle(ActionEvent arg0) {
        RequeteSQL.ajoutClient(this.appli.getConnexionMySQL(),this.fenetreConcepteur,this.appli);
        
    }

}
