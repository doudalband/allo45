import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurSupprimerQuestion implements EventHandler<ActionEvent>{

    private FenetreConcepteur fenetreConcepteur;

    private Question question;

    public ControleurSupprimerQuestion(FenetreConcepteur fenetreConcepteur, Question question) {
    
        this.fenetreConcepteur =fenetreConcepteur;
        this.question = question;
    }

    @Override
    public void handle(ActionEvent event) {
        this.fenetreConcepteur.getQuestionnaireCourant().getListeQuestions().remove(this.question);
        this.fenetreConcepteur.miseAjourAffichage();
        
    }

}
