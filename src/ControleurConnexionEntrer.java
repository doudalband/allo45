import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;



public class ControleurConnexionEntrer implements EventHandler<KeyEvent>{

    FenetreConnexion vueConnexion;




    public ControleurConnexionEntrer( FenetreConnexion vueConnexion) {
        this.vueConnexion = vueConnexion;

    }

    @Override
    public void handle(KeyEvent actionEvent) {
        if(actionEvent.getCode().equals(KeyCode.ENTER))
            this.vueConnexion.getButton().fire();
    }
}