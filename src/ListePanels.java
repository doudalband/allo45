import java.util.ArrayList;
import java.util.List;

public class ListePanels {
    
    private Panel panelCourant;
    private List<Panel> listePan;
    
    public ListePanels(){
        this.listePan = new ArrayList<>();
        this.panelCourant = null;
    }
    public List<Panel> getListePan() {
        return listePan;
    }
    public Panel getPanelCourant() {
        return panelCourant;
    }
    
    public void ajouterPanel(Panel panel){
        this.listePan.add(panel);
    }

    public void setPanelCourant(Panel panelCourant) {
        this.panelCourant = panelCourant;
    }
}
