import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

public class ControleurBoutonDeconnexion implements EventHandler<ActionEvent> {
    
    private AppliPlusieursFenetres appli;

    public ControleurBoutonDeconnexion(AppliPlusieursFenetres appli){
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        //Optional<ButtonType> reponse = this.appli.popUpDeconnection().showAndWait();
            //if (reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                Connexion connexion = appli.getConnexionMySQL();
                try {
                    connexion.close();
                    Stage stage = this.appli.getStage();
                    this.appli = new AppliPlusieursFenetres();
                    this.appli.init();
                    this.appli.start(stage);
                } catch (SQLException e) {
                    e.printStackTrace();
            }
        }
 //   }

     
}
