import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ContoleurNote implements EventHandler<ActionEvent>{

        private FenetreConcepteur fenetreConcepteur;
    
    
        public ContoleurNote(FenetreConcepteur fenetreConcepteur) {
            this.fenetreConcepteur = fenetreConcepteur;
            
    
        }
    
        @Override
        public void handle(ActionEvent actionEvent) {
            String txt = this.fenetreConcepteur.getTfValMax();
            if(!txt.equals(""));
            try{
                this.fenetreConcepteur.setMaxVal(Integer.parseInt(txt));}
            catch(NumberFormatException exception){this.fenetreConcepteur.popUpNbValide().showAndWait();}
        }
    }
