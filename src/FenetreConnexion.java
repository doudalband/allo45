// import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.image.ImageView;


public class FenetreConnexion extends GridPane{


    protected TextField identifiant;
    protected PasswordField password;
    protected Button connexion;
    protected Button boutonAide;

    public FenetreConnexion(Button connexion, Button aide){
        super();
        
        this.connexion = connexion;
        this.boutonAide = aide;
        this.connexion.setStyle("-fx-background-color : #B8D0DD; -fx-font-weight: bold; -fx-background-radius: 50px;");
        this.setStyle("-fx-background-color : #E2F0F8; -fx-background-image: url(\"background2.jpg\");background-repeat: no-repeat;-fx-background-size: cover; ");
        ImageView logoAllo45 = new ImageView("logoALLO45.png");
        logoAllo45.setFitHeight(200);
        logoAllo45.setFitWidth(200);
        logoAllo45.setPreserveRatio(true);
        Label id = new Label("Identifiant");
        id.setStyle("-fx-font-family: Georgia; -fx-font-size : 160%");
        Label pw = new Label("Mot de passe");
        pw.setStyle("-fx-font-family: Georgia; -fx-font-size : 160%");
        this.identifiant =  new TextField();
        identifiant.setStyle("-fx-background-color : #E2F0F8; -fx-font-size:20px;");
        pw.setPadding(new Insets(10));
        id.setPadding(new Insets(10));
        this.password = new PasswordField();
        this.password.setOnKeyPressed(new ControleurConnexionEntrer(this));
        password.setStyle("-fx-background-color : #E2F0F8;-fx-font-size:20px;");
        VBox vboxlogin = new VBox();    
        vboxlogin.setStyle("-fx-background-color : #fcfdfe");
        vboxlogin.getChildren().addAll(id, identifiant, pw, password);
        vboxlogin.setPadding(new Insets(20));
        this.boutonAide.setAlignment(Pos.BASELINE_RIGHT);
        this.boutonAide.setStyle("-fx-background-color : #B8D0DD; -fx-font-weight: bold; -fx-background-radius: 50px;");
        HBox boxBouton = new HBox();
        boxBouton.setStyle("-fx-background-color : #DDEAF1");
        boxBouton.getChildren().addAll(this.connexion, this.boutonAide);
        boxBouton.setSpacing(20);
        VBox Vbbp = new VBox();
        Vbbp.getChildren().add(boxBouton);
        Vbbp.setPadding(new Insets(20));
        Vbbp.setStyle("-fx-background-color : #DDEAF1");
        VBox fenetreConnexion = new VBox();
        HBox boximg = new HBox();
        boximg.setPadding(new Insets(5,0,0,0));
        boximg.setStyle("-fx-background-color : #fcfdfe");
        boximg.getChildren().add(logoAllo45);
        boximg.setAlignment(Pos.CENTER);
        fenetreConnexion.getChildren().addAll(boximg, vboxlogin, Vbbp);
        this.setAlignment(Pos.CENTER);
        fenetreConnexion.setPrefWidth(400);
        this.add(fenetreConnexion, 0, 0);
    }

    public String getIdentifiant(){
        return this.identifiant.getText();
    }

    public String getPassword(){
        return this.password.getText();
    }
    public Button getButton(){
        return this.connexion;
    }
}