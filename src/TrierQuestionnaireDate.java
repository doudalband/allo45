import java.util.Comparator;

import javafx.scene.Node;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class TrierQuestionnaireDate implements Comparator<Node>{

    @Override
    public int compare(Node node1, Node node2) {
        VBox vbox1 = (VBox) node1;
        VBox vbox2 = (VBox) node2;


        TextFlow tf1 = (TextFlow) vbox1.getChildren().get(0);
        Text t1 = (Text) tf1.getChildren().get(0);

        TextFlow tf2 = (TextFlow) vbox2.getChildren().get(0);
        Text t2 = (Text) tf2.getChildren().get(0);

        int num1 = Integer.parseInt(t1.getText().replace(" - ", ""));
        int num2 = Integer.parseInt(t2.getText().replace(" - ", ""));

        return num1-num2;
    }
}