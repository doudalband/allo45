import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class ControleurConnexion implements EventHandler<ActionEvent>{

    FenetreConnexion vueConnexion;

    AppliPlusieursFenetres vueAppli;

    public ControleurConnexion(AppliPlusieursFenetres vueAppli, FenetreConnexion vueConnexion) {
        this.vueAppli = vueAppli;
        this.vueConnexion=vueConnexion;

    }

    @Override
    public void handle(ActionEvent actionEvent) {
        RequeteSQL.seConnecter(this.vueAppli, this.vueConnexion);
    }
}