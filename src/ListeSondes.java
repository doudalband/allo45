import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ListeSondes {
    

    private Sonde sondeCourant;
    private List<Sonde> listeSondes;

    public ListeSondes(){
        this.listeSondes = new ArrayList<>();
        this.sondeCourant = null;
    }

    public boolean clientSuiv(Sonde s) throws NoSuchElementException{
        if(this.listeSondes.size() == 0){throw new NoSuchElementException();}
        int indiceSondeCourant = this.listeSondes.indexOf(s);
        if(indiceSondeCourant != -1 && indiceSondeCourant+1 <= this.listeSondes.size()-1){
            setSondeCourant(this.listeSondes.get(indiceSondeCourant+1));
            return true;
        }
        else{
            return false;
        }
    }

    public boolean clientPrec(Sonde s) throws NoSuchElementException{
        if(this.listeSondes.size() == 0){throw new NoSuchElementException();}
        int indiceSondeCourant = this.listeSondes.indexOf(s);
        if(indiceSondeCourant != -1 && indiceSondeCourant-1 >= 0){
            setSondeCourant(this.listeSondes.get(indiceSondeCourant-1));
            return true;
        }
        else{
            return false;
        }
    }

    public List<Sonde> getListeSondes() {
        return listeSondes;
    }

    public Sonde getSondeCourant() {
        return sondeCourant;
    }

    public void setSondeCourant(Sonde sondeCourant) {
        this.sondeCourant = sondeCourant;
    }

    public void ajouteSonde(Sonde sonde){
        this.listeSondes.add(sonde);
    }

    public void supprimeSonde(Sonde sonde){
        this.listeSondes.remove(sonde);
    }

    @Override
    public String toString(){
        return this.listeSondes.toString();
    }
}
