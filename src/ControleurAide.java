
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Contrôleur à activer lorsque l'on clique sur le bouton info
 */
public class ControleurAide implements EventHandler<ActionEvent> {

    private AppliPlusieursFenetres applifenetre;

    /**
     * @param p vue du jeu
     */
    public ControleurAide(AppliPlusieursFenetres applifenetre) {
        this.applifenetre = applifenetre;
    }

    /**
     * L'action consiste à afficher une fenêtre popup précisant les règles du jeu.
     * @param actionEvent l'événement action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        this.applifenetre.popUpAide().showAndWait();
    }
}