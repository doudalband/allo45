import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class ControleurChoixReponse implements EventHandler<ActionEvent>{

    private FenetreConcepteur fenetreConcepteur;
    


    public ControleurChoixReponse(FenetreConcepteur fenetreConcepteur) {
        this.fenetreConcepteur = fenetreConcepteur;

    }

    @Override
    public void handle(ActionEvent actionEvent) {
        String listeItem = this.fenetreConcepteur.getChoixTypeRep().getValue().toString(); 
        if(listeItem.contains("unique" )){
            this.fenetreConcepteur.setTypeR('u');
            this.fenetreConcepteur.setFenetreLesReponses(this.fenetreConcepteur.boxChoixUnique());
        }
        else if(listeItem.contains("multiples")){
            this.fenetreConcepteur.setTypeR('m');
            this.fenetreConcepteur.setFenetreLesReponses(this.fenetreConcepteur.boxChoixMultiple("Nombre max. de choix"));
        }
        else if(listeItem.contains("Classement")){
            this.fenetreConcepteur.setTypeR('c');
            this.fenetreConcepteur.setFenetreLesReponses(this.fenetreConcepteur.boxChoixClassement("Taille classement"));
        }
        else if(listeItem.contains("Note")){
            this.fenetreConcepteur.setTypeR('n');
            this.fenetreConcepteur.setFenetreLesReponses(this.fenetreConcepteur.boxChoixNote());
        }
        else{
            this.fenetreConcepteur.setTypeR('l');
            this.fenetreConcepteur.boxChoixLibre();
        }
    }
}