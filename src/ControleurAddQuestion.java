import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class ControleurAddQuestion implements EventHandler<ActionEvent>{
    

    private FenetreConcepteur fenetreConcepteur;

    public ControleurAddQuestion(FenetreConcepteur fenetreConcepteur){
        this.fenetreConcepteur = fenetreConcepteur;
    }

    @Override
    public void handle(ActionEvent event){

        if(this.fenetreConcepteur.getComboTypeRep().equals("Choix multiples")){
            this.fenetreConcepteur.valideChoixMultiples();
        }
        else if (this.fenetreConcepteur.getComboTypeRep().equals("Classement")){
            this.fenetreConcepteur.valideClassement();
        }
        else if (this.fenetreConcepteur.getComboTypeRep().equals("Réponse unique")){
            this.fenetreConcepteur.valideChoixUnique();
        }
        else if (this.fenetreConcepteur.getComboTypeRep().equals("Note")){  
            this.fenetreConcepteur.valideNotes();
        }
        else if(this.fenetreConcepteur.getComboTypeRep().equals("Réponse libre")){
            this.fenetreConcepteur.valideChoixLibre();
        }
    }
}
        
