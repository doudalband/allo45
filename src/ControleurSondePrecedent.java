import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class ControleurSondePrecedent implements EventHandler<ActionEvent>{

    private FenetreSondeur vueSond;



    public ControleurSondePrecedent(FenetreSondeur vueSond) {
        this.vueSond = vueSond;
    }


    @Override
    public void handle(ActionEvent actionEvent){
        if(this.vueSond.getQuestionnaire() != null)
            this.vueSond.sondePrec();
    }















}