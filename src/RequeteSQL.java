import java.net.ConnectException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.control.Alert;

public class RequeteSQL {

    public static void saveQuestionnaire(Questionnaire questionnaire, Connexion laConnexion) {
        try {
            laConnexion.connecter();
            java.sql.PreparedStatement ps = laConnexion.prepareStatement("insert into QUESTIONNAIRE values (?,?,?,?,?,?)");
            ps.setInt(1, questionnaire.getIdQuestionnaire());
            ps.setString(2, questionnaire.getNomQuestionnaire());
            ps.setString(3, questionnaire.getEtat()+"");
            ps.setInt(4, questionnaire.getNumClient());
            ps.setInt(5, questionnaire.getIdCreateur());
            ps.setInt(6, questionnaire.getidPan());
            ps.executeUpdate();


            for(Question question : questionnaire.getListeQuestions()){
                java.sql.PreparedStatement ps1 = laConnexion.prepareStatement("insert into QUESTION values (?,?,?,?,?)");
                ps1.setInt(1, questionnaire.getIdQuestionnaire());
                ps1.setInt(2, question.getIdQ());
                ps1.setString(3,question.getTexteQ());
                ps1.setInt(4,question.getMaxVal());
                ps1.setString(5,question.getTypeReponse()+"");
                ps1.executeUpdate();
                for(ValPossible valPossible : question.getListeValPossible()){
                    java.sql.PreparedStatement ps2 = laConnexion.prepareStatement("insert into VALPOSSIBLE values (?,?,?,?)");
                    ps2.setInt(1, questionnaire.getIdQuestionnaire());
                    ps2.setInt(2, question.getIdQ());
                    ps2.setInt(3, valPossible.getIdV());
                    ps2.setString(4, valPossible.getValeur());
                    ps2.executeUpdate();

                }

            }
        }
        catch (SQLException e) {
            
            e.printStackTrace();
        }



    }

    public static void delete(Questionnaire questionnaire, Connexion laConnexion) {

        try {
            laConnexion.connecter();
            java.sql.PreparedStatement ps = laConnexion.prepareStatement("DELETE FROM QUESTIONNAIRE WHERE idQ =" + questionnaire.getIdQuestionnaire());
            java.sql.PreparedStatement ps1 = laConnexion.prepareStatement("DELETE FROM QUESTION WHERE idQ =" + questionnaire.getIdQuestionnaire());    
            java.sql.PreparedStatement ps2 = laConnexion.prepareStatement("DELETE FROM VALPOSSIBLE WHERE idQ =" + questionnaire.getIdQuestionnaire());
            
            
            
            ps2.executeUpdate();
            ps1.executeUpdate();
            ps.executeUpdate();

            

        }
        catch (SQLException e) {
            
            e.printStackTrace();
        }
    }    




    public static void saveReponseQuestion(String val, Question question, Connexion laConnexion, FenetreSondeur fenetreSondeur) {

        try {
            Sonde s = fenetreSondeur.getSondeEnCours();
            laConnexion.connecter();
            java.sql.PreparedStatement ps = laConnexion.prepareStatement("insert into REPONDRE values (?,?,?,?)");
            ps.setInt(1, fenetreSondeur.getQuestionnaire().getIdQuestionnaire());
            ps.setInt(2, question.getIdQ());
            ps.setString(3, s.getCaracteristique().getIdC());
            ps.setString(4, val);
            ps.executeUpdate();
        }
        catch (SQLException e) {
            
            e.printStackTrace();
        }
    }


    public static void sondeARepondu(Connexion laConnexion , FenetreSondeur fenetreSondeur, AppliPlusieursFenetres appli){
        try {
            Sonde s = fenetreSondeur.getSondeEnCours();
            laConnexion.connecter();
            java.sql.PreparedStatement ps1 = laConnexion.prepareStatement("insert into INTERROGER values (?,?,?)");
            ps1.setInt(1, appli.getUser());
            ps1.setInt(2, s.getNumSond());
            ps1.setInt(3, fenetreSondeur.getQuestionnaire().getIdQuestionnaire());
            ps1.executeUpdate();
        }
        catch (SQLException e) {
            
            e.printStackTrace();
        }


    }
    public static void updateQuestionnaireFiniConcepteur(Connexion laConnexion, Questionnaire questionnaire){
       
        try {
            laConnexion.connecter();
            java.sql.PreparedStatement ps = laConnexion.prepareStatement("UPDATE QUESTIONNAIRE SET Etat = 'S' where idQ="+ questionnaire.getIdQuestionnaire());
            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public static void updateQuestionnaireFiniSondeur(Connexion laConnexion,Questionnaire questionnaire){
       
        try {
            laConnexion.connecter();
            java.sql.PreparedStatement ps = laConnexion.prepareStatement("UPDATE QUESTIONNAIRE SET Etat = 'A' where idQ="+ questionnaire.getIdQuestionnaire());
            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void saveQuestion(Connexion laConnexion, Question question, FenetreConcepteur fenetreConcepteur) {

        try {
            laConnexion.connecter();
       
        java.sql.PreparedStatement ps1 = laConnexion.prepareStatement("insert into QUESTION values (?,?,?,?,?)");

        ps1.setInt(1, fenetreConcepteur.getQuestionnaireCourant().getIdQuestionnaire());
        ps1.setInt(2, question.getIdQ());
        ps1.setString(3, question.getTexteQ());
        ps1.setInt(4, question.getMaxVal());
        ps1.setString(5, question.getTypeReponse()+"");
        ps1.executeUpdate();
        } 
        catch (SQLException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
        }
    }

    public static void chargerClient(Connexion laConnexion, AppliPlusieursFenetres appli) throws SQLException{
        Statement s = laConnexion.createStatement();
        ResultSet clients = s.executeQuery("select * from CLIENT");
        while(clients.next()){
            int numClient = clients.getInt(1);
            String raisonSoc = clients.getString(2);
            String adresse1 = clients.getString(3);
            String adresse2 = clients.getString(4);
            String codePostal = clients.getString(5);
            String ville = clients.getString(6);
            String telephone =  clients.getString(7);
            String email = clients.getString(8);
            Client client = new Client(numClient, raisonSoc, adresse1, adresse2, codePostal, ville, telephone, email);
            ResultSet questionnaire_client = s.executeQuery("select * from QUESTIONNAIRE where numC = '"+client.getNumClient()+"' ");
            while(questionnaire_client.next()){
                int idQ = questionnaire_client.getInt(1);
                String titre = questionnaire_client.getString(2);
                Character etat = questionnaire_client.getString(3).charAt(0);
                int idclient = questionnaire_client.getInt(4);
                int createur = questionnaire_client.getInt(5);
                int idPan = questionnaire_client.getInt(6);
                Questionnaire questionnaire = new Questionnaire(idQ, titre, etat,idclient, createur, idPan);
                ResultSet question_questionnaire = s.executeQuery("select * from QUESTION where idQ = '"+questionnaire.getIdQuestionnaire()+"' ");
                while(question_questionnaire.next()){
                    int numQ = question_questionnaire.getInt(2);
                    String texteQ = question_questionnaire.getString(3);
                    int MaxVal = question_questionnaire.getInt(4);
                    Character idT =  question_questionnaire.getString(5).charAt(0);
                    Question question = new Question(numQ, texteQ, MaxVal,idT);
                    ResultSet reponse_question = s.executeQuery("select * from VALPOSSIBLE where numQ = '"+numQ +"' and idQ='" + questionnaire.getIdQuestionnaire()+"'");
                    while(reponse_question.next()){
                        int idV = reponse_question.getInt(3);
                        String valeur = reponse_question.getString(4);
                        ValPossible valeurpossible = new ValPossible(idV, valeur);
                        question.getListeValPossible().add(valeurpossible);
                    }
                    questionnaire.ajouteQuestion(question);
                }   
                client.ajouteQuestionnaire(questionnaire);

                
            }
            appli.getModeleClient().ajouteClient(client);
        }

    }


    public static void chargerPanel(Connexion laConnexion, AppliPlusieursFenetres appli) throws SQLException{
        Statement s = laConnexion.createStatement();
        ResultSet res = s.executeQuery("select * from PANEL");
        while(res.next()){
            int idPan = res.getInt(1);
            String nomPan = res.getString(2);
            Panel panel = new Panel(idPan, nomPan);
            appli.getModelPanel().ajouterPanel(panel);
            ResultSet compositionPanel = s.executeQuery("select numSond from CONSTITUER where idPan ="+idPan);
            while(compositionPanel.next()){
                int numSond = compositionPanel.getInt(1);
                ResultSet compositionClient = s.executeQuery("select * from SONDE where numSond ="+numSond);
                while(compositionClient.next()){
                    String nomSond = compositionClient.getString(2);
                    String prenomSond = compositionClient.getString(3);
                    Date dateNaisSond = compositionClient.getDate(4);
                    String numTelSond = compositionClient.getString(5);
                    String idC = compositionClient.getString(6);
                    Sonde sonde = new Sonde(numSond, nomSond, prenomSond, dateNaisSond, numTelSond, new Caracteristique(idC));
                    panel.ajouterSonde(sonde);
                }

            }
        }
    }
    public static Map<Questionnaire, List<Sonde>> chargerPannelSondeur(AppliPlusieursFenetres appli, Connexion laConnexion) throws SQLException{
        Panel panel = null;
        Map<Questionnaire, List<Sonde>> dico= new HashMap<>();
        for(Client client : appli.getModeleClient().getListeClient()){
            for(Questionnaire questionnaire : client.getListeQuestionnaire()){
                Statement s = laConnexion.createStatement();
                ResultSet res = s.executeQuery("select * from PANEL where idPan ="+questionnaire.getidPan());
                res.next();
                String nomPan = res.getString(2);
                panel = new Panel(questionnaire.getidPan(), nomPan);
                appli.getModelPanel().ajouterPanel(panel);
                ResultSet compositionPanel = s.executeQuery("select numSond from CONSTITUER where idPan ="+questionnaire.getidPan());
                while(compositionPanel.next()){
                    int numSond = compositionPanel.getInt(1);
                    ResultSet compositionClient = s.executeQuery("select * from SONDE where numSond ="+numSond);
                    while(compositionClient.next()){
                        String nomSond = compositionClient.getString(2);
                        String prenomSond = compositionClient.getString(3);
                        Date dateNaisSond = compositionClient.getDate(4);
                        String numTelSond = compositionClient.getString(5);
                        String idC = compositionClient.getString(6);
                        Sonde sonde = new Sonde(numSond, nomSond, prenomSond, dateNaisSond, numTelSond, new Caracteristique(idC));
                        ResultSet pasdedans = s.executeQuery("select * from INTERROGER where numSond = " + numSond + " and idQ = " + questionnaire.getIdQuestionnaire());
                        if(pasdedans.next() == false)
                            panel.ajouterSonde(sonde);
                    }

            }
            if(panel != null){
                List<Sonde> listSondes = new ArrayList<Sonde>(panel.getLesSonde());
                dico.put(questionnaire, listSondes);
        }
       
        }
        
        
        }
    return dico;
    }

    public static void ajoutClient(Connexion connexion, FenetreConcepteur fenetreConcepteur,AppliPlusieursFenetres appli) {

        try{
            String raisonSoc = fenetreConcepteur.getRaisonSocial();
            if(!RequeteSQL.conient(raisonSoc,appli.getModeleClient())){
                String adresse = fenetreConcepteur.getAdresse();
                String complAdresse = fenetreConcepteur.getComplAdresse();
                String codePostal = fenetreConcepteur.getCodePostal().replace(" ", "").replace("'", " ");
                String ville = fenetreConcepteur.getVille();
                String telephone = fenetreConcepteur.getTelephone().replace(" ", "");
                String email = fenetreConcepteur.getEmail().replace(" ", "");

                


                try {
                    connexion.connecter();
                    Statement s = connexion.createStatement();
                    ResultSet res = s.executeQuery("select max(numC) from CLIENT" );
                    res.next();
                    int max = res.getInt(1);
                

                    Client c = new Client(max +1 , raisonSoc, adresse, complAdresse, codePostal, ville, telephone, email);
                    appli.getModeleClient().ajouteClient(c);
                    java.sql.PreparedStatement ps = connexion.prepareStatement("insert into CLIENT values(?,?,?,?,?,?,?,?)");
                    ps.setInt(1,max+1);
                    ps.setString(2,raisonSoc);
                    ps.setString(3, adresse);
                    ps.setString(4, complAdresse);
                    ps.setString(5, codePostal);
                    ps.setString(6, ville);
                    ps.setString(7, telephone);
                    ps.setString(8, email);
                    ps.executeUpdate();
                    fenetreConcepteur.nomClients();
                    fenetreConcepteur.setCentreAjouterQuestionnaire();
                }
                catch(SQLException e){
                    
                }
            }
        }
        catch(Exception error){

        }
        
    }

    public static boolean conient(String raisonSoc ,ListeClients modele){
        for(Client c : modele.getListeClient()){
            if(c.getRaisonSociale() == raisonSoc){
                return true;
            }
        }
        return false;
    }

    public static void seConnecter(AppliPlusieursFenetres vueAppli, FenetreConnexion vueConnexion) {
        Connexion connexion = vueAppli.getConnexionMySQL();
       
        try {
            connexion.connecter();
            java.sql.PreparedStatement s = connexion.prepareStatement("select * from UTILISATEUR where login = ? and motDePasse = ?" );
            String identifiant = vueConnexion.getIdentifiant();
            String password = vueConnexion.getPassword();
            s.setString(1, identifiant);
            s.setString(2,password);
            ResultSet res = s.executeQuery();
            if(res.next()){
                Utilisateur user = new Utilisateur(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getInt(6));
                vueAppli.setUser(user);
                switch(user.getIdRole()){

                    case 1:
                        vueAppli.afficheFenetreConcepteur();
                        break;
                    case 2:
                        vueAppli.afficheFenetreSondeur();
                        break;
                    case 3:
                        vueAppli.afficheFenetreAnalyste();
                        break;
                }    
            }
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Echec !!!! ");
            alert.setResizable(true);
            alert.setWidth(500);
            alert.setHeaderText("Echec de la connexion au serveur");
            alert.setContentText("Voici le message envoyé par le serveur\n"+e.getMessage());
            alert.showAndWait();
        }
    }

    public static void creerSondage(AppliPlusieursFenetres application, FenetreConcepteur fenetreConcepteur,ListeClients modele) {
        try{
            String nomClient = fenetreConcepteur.getComboClient();
            String nomSondage = fenetreConcepteur.getNomSondage();
            if(!nomSondage.equals("") && !nomClient.equals("")){
                try{
                    Connexion connexion = application.getConnexionMySQL();
                    connexion.connecter();
                    int idPan = 0;
                    for(Panel panel : fenetreConcepteur.getListePanels().getListePan()){
                        if(panel.getNomPan().equals(fenetreConcepteur.getComboPane())){
                            idPan = panel.getIdPan();
                        }
                    }
                    for(Client c : modele.getListeClient()){
                        if(c.getRaisonSociale().equals(nomClient)){
                            
                            
                            fenetreConcepteur.getComboPane();
                            Statement s = connexion.createStatement();
                            ResultSet res = s.executeQuery("select max(idQ) from QUESTIONNAIRE");
                            res.next();
                            Questionnaire questionnaire = new Questionnaire(res.getInt(1)+1, nomSondage, c.getNumClient(), application.getUser(), idPan);
                            c.ajouteQuestionnaire(questionnaire);
                            java.sql.PreparedStatement ps = connexion.prepareStatement("insert into QUESTIONNAIRE(idQ, Titre, Etat, numC, idU, idPan) values(?,?,?,?,?,?)");
                            ps.setInt(1, res.getInt(1)+1);
                            ps.setString(2, nomSondage);
                            ps.setString(3, "C");
                            ps.setInt(4, c.getNumClient());
                            ps.setInt(5, application.getUser());
                            ps.setInt(6, idPan); 
                            ps.executeUpdate();
                            fenetreConcepteur.afficheLesQuestions();
                            fenetreConcepteur.setQuestionnaireCourant(questionnaire);
                            application.mettreAjourAffichage();
                    
                            if(fenetreConcepteur.getTrieDate().isSelected()){
                                fenetreConcepteur.trieQuestionnaireParDate();
                            }
                            else{
                                fenetreConcepteur.trieQuestionnaireParEntreprise();

                                
                            }
                            fenetreConcepteur.miseAjourAffichage();
                            break;
                        }
                    }
            }
                catch(SQLException sql){
                    sql.printStackTrace();
                }
            
            }
        }
        catch(Exception e){

        }
        



    }

    public static void setQuestionnaireFini(AppliPlusieursFenetres appli, FenetreConcepteur fenetreConcepteur) {
        Connexion connexion = appli.getConnexionMySQL();
        try {
            connexion.connecter();
            Questionnaire questionnaire = fenetreConcepteur.getQuestionnaireCourant();

            java.sql.Statement s = connexion.createStatement();
            ResultSet res = s.executeQuery("select idQ from QUESTIONNAIRE where idQ = " + questionnaire.getIdQuestionnaire());
            if(res.next() == false){
                RequeteSQL.saveQuestionnaire(questionnaire,appli.getConnexionMySQL());
            }
            int client  = fenetreConcepteur.getQuestionnaireCourant().getNumClient();
            for(Client client_a_chercher : appli.getModeleClient().getListeClient()){
                if(client_a_chercher.getNumClient() == client){
                    appli.getModeleClient().getListeClient().get(appli.getModeleClient().getListeClient().indexOf(client_a_chercher)).ajouteQuestionnaire(questionnaire);
                }
            }
            appli.questionnaire_fini_concepteur(questionnaire);
            fenetreConcepteur.popQuestionnaireFini().showAndWait();
            fenetreConcepteur.resetQuestionnaireCourant();
            fenetreConcepteur.afficheQuestionnaire(appli.chargerSondagePourConcepteur());
            fenetreConcepteur.miseAjourAffichage();

        } catch (SQLException e) {
            
            e.printStackTrace();
        }
    }

    public static void saveQuestionnaireConcepteur(AppliPlusieursFenetres appli, FenetreConcepteur fenetreConcepteur) {
        Connexion connexion = appli.getConnexionMySQL();
        try {
            connexion.connecter();
            Questionnaire questionnaire = fenetreConcepteur.getQuestionnaireCourant();
            java.sql.Statement s = connexion.createStatement();
            ResultSet res = s.executeQuery("select idQ from QUESTIONNAIRE where idQ = " + questionnaire.getIdQuestionnaire());
            if(res.next() == true){
                RequeteSQL.delete(questionnaire,appli.getConnexionMySQL());
            }
            RequeteSQL.saveQuestionnaire(questionnaire,appli.getConnexionMySQL());
            fenetreConcepteur.popQuestionnaireSave().show();;
            fenetreConcepteur.miseAjourAffichage();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
    }

    public static void supprimerQuestionnaire(AppliPlusieursFenetres appli, FenetreConcepteur fenetreConcepteur, ListeClients listeClient) {
        Connexion connexion = appli.getConnexionMySQL();
        try {
            connexion.connecter();
            Questionnaire questionnaire = fenetreConcepteur.getQuestionnaireCourant();
            java.sql.Statement s = connexion.createStatement();
            ResultSet res = s.executeQuery("select idQ from QUESTIONNAIRE where idQ = " + questionnaire.getIdQuestionnaire());
            if(res.next() == true){
                RequeteSQL.delete(questionnaire,appli.getConnexionMySQL());
            }
            for(Client client : listeClient.getListeClient()){
                if(fenetreConcepteur.getQuestionnaireCourant().getNumClient()== client.getNumClient()){
                    client.getListeQuestionnaire().remove(questionnaire);
                }
            }
            fenetreConcepteur.setQuestionnaireCourant(null);
            fenetreConcepteur.resetQuestionnaireCourant();
            fenetreConcepteur.afficheQuestionnaire(appli.chargerSondagePourConcepteur());
            fenetreConcepteur.miseAjourAffichage();
            

        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        
        
    }
    
    
}
