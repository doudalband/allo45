import javafx.event.Event;
import javafx.event.EventHandler;

public class ControleurQuestionCliquee implements EventHandler<Event> {

    private FenetreAnalyste fenetreAnalyste;

    private Question question;

    public ControleurQuestionCliquee(FenetreAnalyste fenetreAnalyste ,Question question) {
        this.fenetreAnalyste= fenetreAnalyste;
        this.question= question;
    }

    @Override
    public void handle(Event arg0) {
        this.fenetreAnalyste.afficheFenetreStatsQuestion(question);
        
    }

}
