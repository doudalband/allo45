import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurBoutonRetourAnalyste implements EventHandler<ActionEvent>{

    private FenetreAnalyste fenetreAnalyste;

    public ControleurBoutonRetourAnalyste(FenetreAnalyste fenetreAnalyste){

        this.fenetreAnalyste = fenetreAnalyste;
    }

    @Override
    public void handle(ActionEvent arg0) {
        this.fenetreAnalyste.afficheLesQuestions();
        
    }

}
