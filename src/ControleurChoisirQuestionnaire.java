import java.util.List;
import java.util.Map;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurChoisirQuestionnaire implements EventHandler<ActionEvent>{
    private FenetreSondeur fenetreSondeur;
    private Questionnaire questionnaire;
    private Map<Questionnaire, List<Sonde>> dicoQP;


    public ControleurChoisirQuestionnaire(FenetreSondeur fenetreSondeur, Questionnaire questionnaire, Map<Questionnaire, List<Sonde>> dicoQP) {
        this.fenetreSondeur = fenetreSondeur;
        this.questionnaire = questionnaire;
        this.dicoQP = dicoQP;



    }

    @Override
    public void handle(ActionEvent actionEvent) {
        this.fenetreSondeur.changerQuestionnaire(this.questionnaire);
        this.fenetreSondeur.setPanel(this.dicoQP.get(this.questionnaire));
        this.fenetreSondeur.miseAjourAffichage();
        

        
        
    }
}

