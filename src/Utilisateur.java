public class Utilisateur {
    
    private int idU;
    private String nom;
    private String prenom;  
    private String login;
    private String motDePasse;
    private int idRole;
    
    public Utilisateur(int idU, String nom, String prenom, String login, String motDePasse, int idRole){
        this.idU = idU;
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
        this.motDePasse = motDePasse;
        this.idRole = idRole;
    }

    // Les Getters
    public int getIdRole() {
        return idRole;
    }

    public int getIdU() {
        return idU;
    }

    public String getLogin() {
        return login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }
    
    //Les Setters
    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public void setIdS(int idU) {
        this.idU = idU;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public String toString(){
        return this.prenom + ", " + this.nom + ", " + this.login + ", " + this.motDePasse + ", " + this.idRole;
    }
}
