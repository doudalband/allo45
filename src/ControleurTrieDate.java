import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurTrieDate implements EventHandler<ActionEvent> {

    private FenetreConcepteur fenetreConcepteur;

    private FenetreAnalyste fenetreAnalyste;

    public ControleurTrieDate(FenetreConcepteur fenetreConcepteur){
        this.fenetreConcepteur = fenetreConcepteur;

    }

    public ControleurTrieDate(FenetreAnalyste fenetreAnalyste) {
        this.fenetreAnalyste = fenetreAnalyste;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        if(fenetreAnalyste == null)
            this.fenetreConcepteur.trieQuestionnaireParEntreprise();
        else
        this.fenetreAnalyste.trieQuestionnaireParEntreprise();
    }
}