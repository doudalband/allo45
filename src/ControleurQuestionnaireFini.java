import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;

public class ControleurQuestionnaireFini implements EventHandler<ActionEvent> {

    private AppliPlusieursFenetres appli;

    private FenetreConcepteur fenetreConcepteur;

    public ControleurQuestionnaireFini(AppliPlusieursFenetres appliPlusieursFenetres,
            FenetreConcepteur fenetreConcepteur) {
                this.appli = appliPlusieursFenetres;
                this.fenetreConcepteur = fenetreConcepteur;
    }

    @Override
    public void handle(ActionEvent event) {
        Optional<ButtonType> reponse = this.fenetreConcepteur.popUpValideQuestionnaire().showAndWait();
            if(reponse.isPresent() && reponse.get().equals(ButtonType.YES)){
                RequeteSQL.setQuestionnaireFini(this.appli, this.fenetreConcepteur);
            }
    }
}
