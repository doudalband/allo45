import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurTriEntreprise implements EventHandler<ActionEvent> {

    private FenetreConcepteur fenetreConcepteur;

    private FenetreAnalyste fenetreAnalyste;

    public ControleurTriEntreprise(FenetreConcepteur fenetreConcepteur){
        this.fenetreConcepteur = fenetreConcepteur;

    }

    public ControleurTriEntreprise(FenetreAnalyste fenetreAnalyste) {
        this.fenetreAnalyste = fenetreAnalyste;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        if(fenetreAnalyste == null)
            this.fenetreConcepteur.trieQuestionnaireParEntreprise();
        else
        this.fenetreAnalyste.trieQuestionnaireParEntreprise();
    }
}