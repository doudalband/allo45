import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class ControleurSupprimerReponse implements EventHandler<ActionEvent>{

    private FenetreConcepteur fenetreConcepteur;    


    public ControleurSupprimerReponse(FenetreConcepteur fenetreConcepteur) {
        this.fenetreConcepteur = fenetreConcepteur;
        

    }

    @Override
    public void handle(ActionEvent actionEvent) {
        this.fenetreConcepteur.supprimeReponse();
    }
}