

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class ControleurCliqueQuestionnaireDroite implements EventHandler<Event> {

    private FenetreConcepteur fenetreConcepteur;

    private Questionnaire questionnaire;

    private VBox vbox;

    private FenetreAnalyste fenetreAnalyste;

    public ControleurCliqueQuestionnaireDroite(FenetreConcepteur fenetreConcepteur,Questionnaire questionnaire,VBox vbox) {
    
        this.fenetreConcepteur = fenetreConcepteur;
        this.questionnaire = questionnaire;
        this.vbox = vbox;
    }

    public ControleurCliqueQuestionnaireDroite(FenetreAnalyste fenetreAnalyste, Questionnaire questionnaire2, VBox vb) {
        this.fenetreAnalyste = fenetreAnalyste;
        this.questionnaire = questionnaire2;
        this.vbox = vb;

    }

    @Override
    public void handle(Event arg0) {

        this.vbox.requestFocus();
        if(this.fenetreAnalyste == null){
            this.fenetreConcepteur.setQuestionnaireCourant(questionnaire);
            this.fenetreConcepteur.miseAjourAffichage();
            this.fenetreConcepteur.afficheLesQuestions();
            this.fenetreConcepteur.clearCouleur();
            this.vbox.setStyle("-fx-background-color:#2498D9;-fx-cursor:hand");
            for(Node enfant : this.vbox.getChildren()){
                TextFlow texts = (TextFlow) enfant;
                for(Node text : texts.getChildren()){
                    Text questionnaire = (Text) text;
                    questionnaire.setFill(Color.WHITE);
                }
                    
            }
        }else{
            this.fenetreAnalyste.setQuestionnaireCourant(questionnaire);
            this.fenetreAnalyste.miseAjourAffichage();
            this.fenetreAnalyste.afficheLesQuestions();
            this.fenetreAnalyste.clearCouleur();
            this.vbox.setStyle("-fx-background-color:#2498D9;-fx-cursor:hand");
            for(Node enfant : this.vbox.getChildren()){
                TextFlow texts = (TextFlow) enfant;
                for(Node text : texts.getChildren()){
                    Text questionnaire = (Text) text;
                    questionnaire.setFill(Color.WHITE);
                }
            }
        }
        

    }
    
    


}
