import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurAnnulerCreationQuestion implements EventHandler<ActionEvent> {


    private FenetreConcepteur fenetreConcepteur;

    public ControleurAnnulerCreationQuestion(FenetreConcepteur fenetreConcepteur) {
        this.fenetreConcepteur = fenetreConcepteur;
    }

    @Override
    public void handle(ActionEvent arg0) {
        this.fenetreConcepteur.afficheLesQuestions();
        
    }

}
