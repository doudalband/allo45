import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class ControleurAjouterReponse implements EventHandler<ActionEvent>{

    private FenetreConcepteur fenetreConcepteur;


    public ControleurAjouterReponse(FenetreConcepteur fenetreConcepteur) {
        this.fenetreConcepteur = fenetreConcepteur;
        

    }

    @Override
    public void handle(ActionEvent actionEvent) {
        try{
            this.fenetreConcepteur.setFenetreLesReponsesUniq();}
            
        catch(Exception exception){this.fenetreConcepteur.popMaxQuestion().showAndWait();}
        
    }
}