
public class Caracteristique {
    
    private String idC;
    private char sexe;
    private char idTr;
    private char idCat;

    public Caracteristique(String idC){
        this.idC = idC;
        this.sexe = idC.charAt(0);
        this.idCat = idC.charAt(1);
        this.idTr = idC.charAt(2);
    }

    public String getIdC() {
        return this.idC;
    }
    

    public char getSexe() {
        return this.sexe;
    }

    public char getidTr(){
        return this.idTr;
    }

    public char getCat(){
        return this.idCat;
    }

    public void setIdC(String idC) {
         this.idC = idC;
    }

    public void setSexe(char sexe) {
        this.sexe = sexe;
    }
    
}
