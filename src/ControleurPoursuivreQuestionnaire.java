import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurPoursuivreQuestionnaire implements EventHandler<ActionEvent> {

    private FenetreSondeur fenetreSondeur;

    public ControleurPoursuivreQuestionnaire(FenetreSondeur fenetreSondeur) {
        this.fenetreSondeur = fenetreSondeur;
    }

    @Override
    public void handle(ActionEvent arg0) {
        if(this.fenetreSondeur.getQuestionnaire() != null)
            this.fenetreSondeur.passeOngletSuivant();
        
    }

}
