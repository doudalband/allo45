import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurCreerQuestionnaire implements EventHandler<ActionEvent>{

    private FenetreConcepteur fenetreConcepteur;


    public ControleurCreerQuestionnaire(FenetreConcepteur fenetreConcepteur){
        this.fenetreConcepteur = fenetreConcepteur;
    }

    @Override
    public void handle(ActionEvent event){
        this.fenetreConcepteur.setCentreAjouterQuestionnaire();
    }
    
}
