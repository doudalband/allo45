import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;



public class FenetreSondeur extends BorderPane {
    
    private Button bActualiser;
    private Label labelNom;    
    private Label labelPrenom;
    private Label labelAge;
    private Label labelTel;
    private Label labelSexe;
    private TabPane tabQuestion;
    private Tab tabIntro;
    private ScrollPane scrollP;
    private TextField tfRecherche;
    private Map<Question, List<Node>> dicoReponses;
    private Sonde sondeEnCour;
    private Map<Questionnaire, List<Sonde>> dicoQP;
    private Map<Questionnaire, Integer> indiceDuSonde;
    private List<Sonde> panelEnCours;
    private Questionnaire questionnaireEnCours;
    private Button bRepondre;
    private Label restant;



    public FenetreSondeur(Button bActualiser, TextField tfRecherche, Button bRepondre){
        this.bRepondre = bRepondre;
        this.restant = new Label();
        this.questionnaireEnCours = null;
        this.indiceDuSonde = new HashMap<>();
        this.panelEnCours = null;
        this.dicoQP = null;
        this.sondeEnCour = null;
        this.dicoReponses = new HashMap<>();
        this.tabQuestion = new TabPane();
        this.tabIntro = new  Tab();
        this.bActualiser = bActualiser;
        this.tfRecherche = tfRecherche;
        this.scrollP = new ScrollPane();
        this.scrollP.setVbarPolicy(javafx.scene.control.ScrollPane.ScrollBarPolicy.AS_NEEDED);
        this.scrollP.setFitToHeight(true);
        this.scrollP.setFitToWidth(true);
        this.getStyleClass().add("this");
        this.tabQuestion = new TabPane();
        this.tabIntro = new Tab("Introduction");
        this.tabQuestion.getStyleClass().add("tabQuestion");
        this.tabQuestion.setTabMinHeight(30);
        this.tabQuestion.getTabs().add(this.tabIntro);
        this.tabIntro.setClosable(false);
        this.tabIntro.getStyleClass().add("tabIntro");
        this.labelNom = new Label();
        this.labelPrenom = new Label();
        this.labelAge = new Label();
        this.labelTel = new Label();
        this.labelSexe = new Label();


        this.tfRecherche.getStyleClass().add("tfRecherche");
        this.tfRecherche.setPromptText("Rechercher un sujet avec un mot clé...");
        this.tfRecherche.setPrefHeight(35);
        this.tfRecherche.setMaxWidth(1200);
        this.setCenter(center());

        
    }

    public void miseAjourAffichage(){
        changerQuestionnaire(this.questionnaireEnCours);
        this.restant.setText("Il reste " +this.panelEnCours.size());

    }

    public void setSonde(){
        this.sondeEnCour = this.panelEnCours.get(this.indiceDuSonde.get(this.questionnaireEnCours));
        this.majAffSonde();
    
    }

    public void setPanel(List<Sonde> p){
        this.panelEnCours = p;
        this.setSonde();
    }

    public void setQuestionnaire(Questionnaire q){
        this.questionnaireEnCours = q;
    }

    public void sondeSuiv(){
        if(this.indiceDuSonde.get(this.questionnaireEnCours) < this.panelEnCours.size()-1){
            this.indiceDuSonde.put(this.questionnaireEnCours,this.indiceDuSonde.get(this.questionnaireEnCours)+1);
            this.setSonde();
        }   
        
    }

    public void sondePrec(){
        if(this.indiceDuSonde.get(this.questionnaireEnCours)>0){
            this.indiceDuSonde.put(this.questionnaireEnCours,this.indiceDuSonde.get(this.questionnaireEnCours)-1);
            this.setSonde();
        }
        
    }

    public Sonde getSondeEnCours(){
        return this.sondeEnCour;
    }

    public Map<Question, List<Node>> getDicoReponse (){
        return this.dicoReponses;
    }

    public Questionnaire getQuestionnaire(){
        return this.questionnaireEnCours;
    }

    public Alert popUpValideToutRepondu(){
        Alert alert = new Alert(Alert.AlertType.WARNING, "Vous devez répondre à toutes les questions");
        alert.setTitle("Répondre à toutes les questions");
        return alert;
    }


    public void retirerSondeurDuPannel(){
        try{
        this.panelEnCours.remove(((int) this.indiceDuSonde.get(this.questionnaireEnCours)));
        this.indiceDuSonde.put(this.questionnaireEnCours,this.indiceDuSonde.get(this.questionnaireEnCours)-1);
        this.sondeSuiv();
        }
        catch(IndexOutOfBoundsException e){
            
        }
        
        
        
    }
    public void majAffSonde(){
        this.labelNom.setText(this.sondeEnCour.getNomSond());
        this.labelPrenom.setText(this.sondeEnCour.getPrenomSond());
        this.labelAge.setText(this.sondeEnCour.getDateNaisSond().toString());
        this.labelTel.setText(this.sondeEnCour.getTelephoneSond());
        this.labelSexe.setText(String.valueOf(this.sondeEnCour.getCaracteristique().getSexe()));
    }
    public void clearLabel() {
        this.labelNom.setText("");
        this.labelPrenom.setText("");
        this.labelAge.setText("");
        this.labelTel.setText("");
        this.labelSexe.setText("");
    }

    public VBox center(){
        VBox vb = new VBox();
        vb.getChildren().addAll(center1(), center2());
        return vb;
    }

    public VBox center1(){
        GridPane millieu = new GridPane();
        millieu.setAlignment(Pos.CENTER);
        millieu.setHgap(80);
        millieu.setVgap(40);
        millieu.setPadding(new Insets(40));
        Label q1 = new Label("Nom");
        Label q2 = new Label("Prénom");
        Label q3 = new Label("Date de naissance");
        Label q4 = new Label("Numéro de téléphone");
        Label q5 = new Label("Sexe");
        List<Label> l1 = Arrays.asList(this.labelNom,q1,this.labelPrenom,q2,this.labelAge,q3,this.labelTel,q4,this.labelSexe,q5);
        for(int i=0; i<l1.size(); i+=2){
            l1.get(i+1).getStyleClass().add("l1Bis");
            l1.get(i).getStyleClass().add("l1");
            VBox v1 = new VBox();
            VBox v2 = new VBox();
            v2.setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, null, null)));
            v2.getChildren().add(l1.get(i));
            v1.getChildren().addAll(l1.get(i+1), v2);
            v1.setSpacing(10);
            v1.setMaxWidth(1000);
            millieu.add(v1, i%6, ((int) i/6));
        }
        Button bPoursuivre = new Button("Poursuivre");
        bPoursuivre.setOnAction(new ControleurPoursuivreQuestionnaire(this));
        bPoursuivre.getStyleClass().add("bPoursuivre");
        millieu.add(bPoursuivre, 2, 3);
        Button bSondeSuivant = new Button("Suivant");
        Button bSondePrecedent = new Button("Précédent");
        bSondePrecedent.getStyleClass().add("bPoursuivre"); 
        bSondeSuivant.getStyleClass().add("bPoursuivre");
        millieu.add(bSondeSuivant, 3, 3);
        millieu.add(bSondePrecedent, 1, 3);
        bSondeSuivant.setOnAction(new ControleurSondeSuivant(this));
        bSondePrecedent.setOnAction(new ControleurSondePrecedent(this));
        VBox center = new VBox();
        center.setPadding(new Insets(30));
        this.tabIntro.setContent(millieu);
        center.getChildren().add(this.tabQuestion);
        return center;
    }


    public void changerQuestionnaire(Questionnaire questionnaire){
        this.questionnaireEnCours = questionnaire;
        this.tabQuestion.getTabs().clear();
        this.tabQuestion.getTabs().add(this.tabIntro);
        Tab tabQuest = new Tab("Questions");
        tabQuest.setClosable(false);
        VBox lesQuestions = new VBox();
        for (Question q : questionnaire.getListeQuestions()){
            List<Node> rep = new ArrayList<>();
            Label uneQuestion = new Label(q.getTexteQ());
            HBox box = new HBox(uneQuestion);
            uneQuestion.setFont(Font.font("Arial", FontWeight.BOLD, 18));
            box.setPadding(new Insets(10));
            box.setSpacing(10);
            if (q.getTypeReponse().equals('l')){
                TextField tf = new TextField();
                box.getChildren().add(tf);
                rep.add(tf);
            }
            else if (q.getTypeReponse().equals('n')){
                ComboBox<Integer> cb = new ComboBox<>();
                ObservableList<Integer> list = FXCollections.observableArrayList();
                for (Integer i=0; i<= q.getMaxVal(); ++i){
                    list.add(i);
                }
                cb.setItems(list);
                rep.add(cb);
                box.getChildren().add(cb);
            }
            else if (q.getTypeReponse().equals('u')){
                ToggleGroup group = new ToggleGroup();
                for (int i=0; i<q.getListeValPossible().size(); ++i){
                    RadioButton rb = new RadioButton(q.getListeValPossible().get(i).getValeur());
                    rb.setStyle("-fx-text-fill: black;");
                    rb.setToggleGroup(group);
                    rep.add(rb);
                    box.getChildren().add(rb);
                }
                
            }
            else if (q.getTypeReponse().equals('m')){
                List<CheckBox> checkBoxes = new ArrayList<CheckBox>();
                ChangeListener<Boolean> listener = new ControleurNbrReponseMaxQcm(q, checkBoxes);
                for (int i=0; i<q.getListeValPossible().size(); ++i){
                    CheckBox cb = new CheckBox(q.getListeValPossible().get(i)+"");
                    cb.selectedProperty().addListener(listener);
                    cb.setStyle("-fx-text-fill: black;");
                    rep.add(cb);
                    box.getChildren().add(cb);
                    checkBoxes.add(cb);
                }

            }
            else if (q.getTypeReponse().equals('c')){
                TilePane tp = new TilePane();
                tp.setPrefColumns(6);
                tp.setVgap(10);
                Label titre = new Label("Séléctionner de votre préféré à la pire");
                titre.setFont(Font.font("Arial", FontWeight.NORMAL, 15));
                titre.setStyle("-fx-text-fill: black;");
                box.getChildren().add(titre);
                for (int i=1; i<=q.getMaxVal(); ++i){
                    ComboBox<String> cb = new ComboBox<>();
                    ObservableList<String> list = FXCollections.observableArrayList();
                    for (ValPossible v: q.getListeValPossible()){
                        list.add(v.getValeur());
                    }
                    cb.setItems(list);
                    rep.add(cb);
                    tp.getChildren().addAll(new Label(i+":"),cb);
                }
                box.getChildren().add(tp);
            
            }
            this.dicoReponses.put(q, rep);
            lesQuestions.getChildren().add(box);
        }
        HBox hbBoutonRepondre = new HBox(10);
        hbBoutonRepondre.getChildren().addAll(this.bRepondre, this.restant);
        this.restant.setFont(Font.font("Arial", FontWeight.NORMAL, 15));
        this.restant.setStyle("-fx-text-fill: black;");
        this.bRepondre.setStyle("-fx-background-color : #2498D9; -fx-font-weight: bold; -fx-background-radius: 500px; -fx-text-base-color: White; -fx-font-size : 13pt; -fx-font-family : \"Arial\";");
        lesQuestions.getChildren().add(hbBoutonRepondre);
        lesQuestions.setPadding(new Insets(10));
        hbBoutonRepondre.setAlignment(Pos.CENTER);
        tabQuest.setContent(lesQuestions);
        
        this.tabQuestion.getTabs().add(tabQuest);
        
    }

    
    
    public BorderPane center2(){
        BorderPane millieu = new BorderPane();
        millieu.setPadding(new Insets(40));
        HBox hbTitre = new HBox();
        hbTitre.getStyleClass().add("hbTitre");
        Label titre = new Label("Thèmes / Sujets");
        titre.getStyleClass().add("titre");
        titre.setAlignment(Pos.CENTER);
        titre.setPadding(new Insets(10, 0, 10, 20));
        hbTitre.getChildren().add(titre);
        titre.setTextFill(Color.web("#FFFFFF"));
        millieu.setTop(hbTitre);
        VBox centre = new VBox();
        centre.getStyleClass().add("centre");
        centre.setSpacing(50);
        centre.setPadding(new Insets(20));
        BorderPane bp = new BorderPane();
        bActualiser.getStyleClass().add("bAjouterSujet");
        bActualiser.setPrefHeight(35);
        bActualiser.setPrefWidth(250);
        bp.setCenter(this.tfRecherche);
        bp.setRight(bActualiser);
        centre.getChildren().addAll(bp, this.scrollP);
        millieu.setCenter(centre);
        return millieu;
    }

    public Button getBoutonActualiser(){
        return this.bActualiser;
    }


    public void mettreSondage(Map<Questionnaire, Client> dico, Map<Questionnaire, List<Sonde>> dicoQP){
        this.dicoQP = dicoQP;
        GridPane gp = new GridPane();
        gp.getStyleClass().add("gridPaneQuestionnaire");
        gp.setHgap(400);
        gp.setVgap(20);
        int i = 0;
        for (Map.Entry<Questionnaire,Client> mapentry : dico.entrySet()){
            this.indiceDuSonde.put(mapentry.getKey(), 0);
            Button bScroll = new Button("Choisir");
            bScroll.getStyleClass().add("bScroll");
            bScroll.setOnAction(new ControleurChoisirQuestionnaire(this, mapentry.getKey(), dicoQP));
            Label lab1 = new Label(mapentry.getKey().getNomQuestionnaire());
            Label lab2 = new Label(mapentry.getValue().getRaisonSociale());
            lab1.getStyleClass().add("labClient");
            lab2.getStyleClass().add("labClient");
            gp.add(lab1, 0, i);
            gp.add(lab2, 1, i);
            gp.add(bScroll, 2, i);
            i += 1;
        }
        this.scrollP.setContent(gp);
    }

    public void mettreLesSondageRecherche(Map<Questionnaire, Client> dico){
        GridPane gp = new GridPane();
        gp.getStyleClass().add("gridPaneQuestionnaire");
        gp.setHgap(400);
        gp.setVgap(20);
        int i = 0;
        for (Map.Entry<Questionnaire,Client> mapentry : dico.entrySet()){
            if (mapentry.getKey().getNomQuestionnaire().contains(this.tfRecherche.getText()) || mapentry.getValue().getRaisonSociale().contains(this.tfRecherche.getText())){
                Button bScroll = new Button("Choisir");
                bScroll.getStyleClass().add("bScroll");
                bScroll.setOnAction(new ControleurChoisirQuestionnaire(this, mapentry.getKey(), this.dicoQP));
                Label lab1 = new Label(mapentry.getKey().getNomQuestionnaire());
                Label lab2 = new Label(mapentry.getValue().getRaisonSociale());
                lab1.getStyleClass().add("labClient");
                lab2.getStyleClass().add("labClient");
                gp.add(lab1, 0, i);
                gp.add(lab2, 1, i);
                gp.add(bScroll, 2, i);
                i += 1;
            }
        }
        this.scrollP.setContent(gp);
    }

    public void passeOngletSuivant() {

        tabQuestion.getSelectionModel().selectNext();
        
    }

    public void premierOnglet(){
        this.tabQuestion.getSelectionModel().selectFirst();
    }
    
    public List<Sonde> getPanelEnCours(){
        return this.panelEnCours;
    }

}
