import java.util.ArrayList;
import java.util.List;

public class Panel {
    
    private int idPan;
    private String nomPan;
    private List<Sonde> panelSonde;

    public Panel(int idPan, String nomPan){
        this.idPan = idPan;
        this.nomPan = nomPan;
        this.panelSonde = new ArrayList<>();
    }

    public int getIdPan() {
        return idPan;
    }


    public String getNomPan() {
        return nomPan;
    }

    public  void setIdPan(int idPan) {
        this.idPan = idPan;
    }
    
    public void setNomPan(String nomPan) {
        this.nomPan = nomPan;
    }

    public void ajouterSonde(Sonde s){
        this.panelSonde.add(s);
    }

    public List<Sonde> getLesSonde(){
        return this.panelSonde;
    }
}
