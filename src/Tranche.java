public class Tranche  {
    
    static char idT;
    private int ageMin;
    private int ageMax;

    public Tranche(char idT, int ageMin, int ageMax){
        idT = Character.forDigit(Character.getNumericValue(idT)+1, 10);
        this.ageMin = ageMin;
        this.ageMax = ageMax;
    }

    public int getAgeMax() {
        return ageMax;
    }

    public int getAgeMin() {
        return ageMin;
    }

    public void setAgeMax(int ageMax) {
        this.ageMax = ageMax;
    }

    public void setAgeMin(int ageMin) {
        this.ageMin = ageMin;
    }
}
