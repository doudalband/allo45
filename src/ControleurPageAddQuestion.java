import javafx.event.ActionEvent;
import javafx.event.EventHandler;


/**
 * Contrôleur à activer lorsque l'on clique sur le bouton ajouter un nouveau client
 */

public class ControleurPageAddQuestion implements EventHandler<ActionEvent>{

    private FenetreConcepteur vueConcep;


    public ControleurPageAddQuestion(FenetreConcepteur vueConcep) {
        this.vueConcep = vueConcep;
    }


    @Override
    public void handle(ActionEvent actionEvent){
        this.vueConcep.setCentreAjouterQuestion();
    }















}