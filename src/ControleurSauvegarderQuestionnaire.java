import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurSauvegarderQuestionnaire implements EventHandler<ActionEvent> {

    private AppliPlusieursFenetres appli;

    private FenetreConcepteur fenetreConcepteur;

    public ControleurSauvegarderQuestionnaire(AppliPlusieursFenetres appliPlusieursFenetres,
            FenetreConcepteur fenetreConcepteur) {

                this.appli = appliPlusieursFenetres;
                this.fenetreConcepteur = fenetreConcepteur;
    }

    @Override
    public void handle(ActionEvent event) {
        RequeteSQL.saveQuestionnaireConcepteur(this.appli, this.fenetreConcepteur);
        
        
    }

}
