import java.awt.Dimension;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class AppliPlusieursFenetres extends Application {
    
    private Button btnconnexion;
    private Button btaide;
    private Scene scene;
    private Utilisateur user;
    private Connexion laConnexion;
    private FenetreConnexion fenetreConnexion;
    private FenetreConcepteur fenetreConcepteur;
    private FenetreAnalyste fenetreAnalyste;
    private FenetreSondeur fenetreSondeur;
    private ListeClients listeClient;
    private ListePanels listePanels;
    private Stage stage;
    private Button bActualiser;
    private TextField tfRecherche;

    private Button btnValiderClient;
    private BorderPane affichage;
    private Button btnConfirmerCreationSondage;
    private Button btvaliderQuestion;
    private ImageView imgDeco;
    private Button btnDeconnexion;
    private MenuButton questionnaire_save;
    private ImageView imgonglet;
    private Button bRepondre;



    /**
     * @param args the command line arguments
     * @throws SQLException
     */
    
    @Override
    public void init() throws SQLException{
        try {
            this.laConnexion = new Connexion();
        }catch (ClassNotFoundException ex){
             System.out.println("Driver MySQL non trouvé!!!");
        }
        this.imgDeco = new ImageView("deco.png");
        this.btnDeconnexion = new Button("", this.imgDeco);
        this.btnDeconnexion.setTooltip(new Tooltip("Se deconnecter"));

        this.imgonglet = new ImageView("onglet.png");
        this.questionnaire_save = new MenuButton("", this.imgonglet);

        this.listeClient = new ListeClients();
        this.listePanels = new ListePanels();
        this.btnconnexion = new Button("SE CONNECTER");
        this.btaide = new Button("AIDE");
        
        this.affichage = new BorderPane();
        this.tfRecherche = new TextField();
        this.bActualiser = new Button("Actualiser");
        this.bRepondre = new Button("Répondre");
        this.fenetreAnalyste = new FenetreAnalyste(this.listeClient,this.laConnexion);
        this.fenetreSondeur = new FenetreSondeur(this.bActualiser, this.tfRecherche, bRepondre);
        this.bActualiser.setOnAction(new ControleurActualiser(this, this.fenetreSondeur));
        this.bRepondre.setOnAction(new ControleurRepondre(this, this.fenetreSondeur));

        this.tfRecherche.setOnKeyPressed(new ControleurRechercherQuestionnaire(this, this.fenetreSondeur));
        this.btnValiderClient = new Button("Valider");

        this.fenetreConnexion = new FenetreConnexion(btnconnexion, this.btaide);
        this.btnConfirmerCreationSondage = new Button("Créer le sondage");
        this.btvaliderQuestion = new Button("Valider Question");
        
        this.fenetreConcepteur = new FenetreConcepteur(this.listeClient, this.btnValiderClient, this.btnConfirmerCreationSondage, this.listePanels, this.btvaliderQuestion,this.questionnaire_save);
        this.btvaliderQuestion.setOnAction(new ControleurAddQuestion(this.fenetreConcepteur));

        this.btnValiderClient.setOnAction(new ControleurAjouterClient(this,listeClient, this.fenetreConcepteur));
        
        this.btnConfirmerCreationSondage.setOnAction(new ControleurCreerSondage(fenetreConcepteur,this,listeClient));
        this.btnconnexion.setOnAction(new ControleurConnexion(this,this.fenetreConnexion));
        this.btaide.setOnAction(new ControleurAide(this));
        this.btnDeconnexion.setOnAction(new ControleurBoutonDeconnexion(this));

    

    }
    public Map<Questionnaire,Client> chargerSondagePourConcepteur() {
        Map<Questionnaire,Client> res = new HashMap<>();
        int idUser = user.getIdU();
        for(Client client : this.listeClient.getListeClient()){
            for(Questionnaire questionnaire : client.getListeQuestionnaire()){
                if(idUser == questionnaire.getIdCreateur() && questionnaire.getEtat().equals('C'))
                    res.put(questionnaire,client);

            }
        }
        return res;
    }

    public Map<Questionnaire,Client> chargerSondage() {
        Map<Questionnaire,Client> res = new HashMap<>();
        for(Client client : this.listeClient.getListeClient()){
            for(Questionnaire questionnaire : client.getListeQuestionnaire()){
                if(questionnaire.getEtat().equals('S'))
                    res.put(questionnaire,client);

            }
        }
        return res;
    }

    public Map<Questionnaire,Client> chargerSondagePouranalyste() {
        Map<Questionnaire,Client> res = new HashMap<>();
        for(Client client : this.listeClient.getListeClient()){
            for(Questionnaire questionnaire : client.getListeQuestionnaire()){
                if(questionnaire.getEtat().equals('A'))
                    res.put(questionnaire,client);

            }
        }
        return res;
    }

    public ListeClients getModeleClient(){
        return this.listeClient;
    }


    public Connexion getConnexionMySQL() {
        return laConnexion;
    }

    @Override
    public void start(Stage stage){
        Pane root = this.fenetreConnexion;
        // Pane root = this.fenetreAnalyste;
        // this.fenetreAnalyste.setLeft(leftAsside());
        this.stage = stage;
        this.scene = new Scene(root, 800, 600);
        this.stage.setScene(scene);
        this.stage.setTitle("Application Allo45");
        this.stage.setResizable(true);
        this.stage.show();
    }

    public void setUser(Utilisateur user){
        this.user = user;
        
    }

    public void setAffichage(){
        this.affichage.setLeft(null);
        this.affichage.setCenter(null);
    }

    

    public void afficheFenetreConnexion(){

        Pane root = new FenetreConnexion(this.btnconnexion, this.btaide);
        this.scene = new Scene(root, 800, 600);
        this.stage.setScene(this.scene);
        this.stage.setResizable(false);
    }

    

    public void afficheFenetreConcepteur(){
        this.stage.setResizable(true);
        try {
            RequeteSQL.chargerClient(laConnexion, this);
            RequeteSQL.chargerPanel(laConnexion, this);
        } catch (SQLException e) {
        }
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int) dimension.getHeight()-50;
        int width = (int) dimension.getWidth();

        this.affichage.setLeft(leftAsside());
        this.affichage.setCenter(this.fenetreConcepteur);
        Pane root = this.affichage;
        
        this.scene = new Scene(root, width, height);
        this.scene.getStylesheets().add(getClass().getResource("concepteur.css").toExternalForm());
        this.fenetreConcepteur.miseAjourAffichage();
        this.fenetreConcepteur.nomClients();
        this.fenetreConcepteur.nomPanel();
        this.fenetreConcepteur.afficheQuestionnaire(chargerSondagePourConcepteur());
        this.fenetreConcepteur.trieQuestionnaireParDate();
        this.stage.setScene(this.scene);
    }

    public int getUser() {
        return this.user.getIdU();
    }
    public Stage getStage(){
        return this.stage;
    }

    public void afficheFenetreSondeur(){
        Map<Questionnaire, List<Sonde>> dic = null;
        this.stage.setResizable(true);
        try {
            RequeteSQL.chargerClient(laConnexion, this);
            dic = RequeteSQL.chargerPannelSondeur(this, laConnexion);
        } catch (SQLException e) {
        }
        this.affichage.setLeft(leftAsside());
        this.affichage.setCenter(this.fenetreSondeur);
        Pane root = this.affichage;

        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int) dimension.getHeight()-50;
        int width = (int) dimension.getWidth(); 
        this.scene = new Scene(root, width, height);

        this.scene.getStylesheets().clear();
        this.scene.getStylesheets().add(getClass().getResource("sondeur.css").toExternalForm());
        this.fenetreSondeur.mettreSondage(this.chargerSondage(), dic);
        
        this.stage.setScene(this.scene);

    }
    
    public void afficheFenetreAnalyste(){
        this.stage.setResizable(true);
        try {
            RequeteSQL.chargerClient(laConnexion, this);
            RequeteSQL.chargerPanel(laConnexion, this);
        } catch (SQLException e) {
        }
        Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int height = (int) dimension.getHeight()-50;
        int width = (int) dimension.getWidth();

        this.affichage.setLeft(leftAsside());
        this.affichage.setCenter(this.fenetreAnalyste);
        Pane root = this.affichage;
        
        this.scene = new Scene(root, width, height);


        this.fenetreAnalyste.nomClients();
        this.fenetreAnalyste.nomPanel();
        this.fenetreAnalyste.afficheQuestionnaire(chargerSondagePouranalyste());
        this.fenetreAnalyste.miseAjourAffichage();
        this.fenetreAnalyste.trieQuestionnaireParDate();
        this.stage.setScene(this.scene);
    }


    public Alert popMaxQuestion(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Limite de question atteinte\n Vous étes limité à 16 questions");
        alert.setTitle("Limite Atteinte");
        return alert;
    }
    public Alert popUpAide(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Si vous avez un problème contactez nous \n À l'Adresse suivante: allo45@help.com");
        alert.setTitle("Aide");
        return alert;
    }

    public Alert popUpDeconnection(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Voulez-vous vraiment vous déconnecter ?", ButtonType.NO, ButtonType.YES );
        alert.setTitle("Déconnection");
        return alert;
    }


    public VBox leftAsside(){
        VBox leftAsside = new VBox();
        leftAsside.setAlignment(Pos.TOP_CENTER);
        leftAsside.setStyle("-fx-background-color : #2498D9");

        // Liste des images utilisé par le module concepteur
        List<String> listeImg = new ArrayList<>(Arrays.asList("onglet.png", "p.png",
        "msg.png", "account.png", "setting.png",
        "help.png"));

        List<String> listInfoBulle = new ArrayList<>(Arrays.asList("Menu", "Documentation", "Messagerie", "Profil", "Parametre", "Aide"));
        int cpt =0;
        ImageView allo45 = new ImageView("logoALLO45.png");
        allo45.setFitHeight(111);
        allo45.setFitWidth(111);
        allo45.setPreserveRatio(true);
        this.imgDeco.setFitHeight(42);
        this.imgDeco.setFitWidth(42);
        this.imgDeco.setPreserveRatio(true);

        this.imgonglet.setFitHeight(42);
        this.imgonglet.setFitWidth(42);
        this.imgonglet.setPreserveRatio(true);


        Button acceuil = new Button("", allo45);
        acceuil.setPadding(new Insets(2));
        this.btnDeconnexion.setPadding(new Insets(19, 0, 19, 0));
        acceuil.setStyle("-fx-background-color : #2498D9");
        this.btnDeconnexion.setStyle("-fx-background-color : #2498D9;-fx-cursor:hand;");

        // acceuil.setPadding(new Insets(10));
        leftAsside.getChildren().add(acceuil);

        for(String urlImg : listeImg){
            ImageView image = new ImageView(urlImg);
            image.setFitHeight(42);
            image.setFitWidth(42);
            image.setPreserveRatio(true);
            if(urlImg.equals("onglet.png") && this.user.getIdRole() == 1){
                MenuItem save_questionnaire = new MenuItem("Sauvegarder le questionnaire");
                MenuItem questionnaire_fini = new MenuItem("Valider le questionnaire");
                MenuItem delete_questionnaire = new MenuItem("Supprimer le questionnaire");
                delete_questionnaire.setDisable(true);
                save_questionnaire.setDisable(true);
                questionnaire_fini.setDisable(true);
                delete_questionnaire.setOnAction(new ControleurSupprimerQuestionnaire(this,this.fenetreConcepteur,this.listeClient));
                questionnaire_fini.setOnAction(new ControleurQuestionnaireFini(this,this.fenetreConcepteur));
                save_questionnaire.setOnAction(new ControleurSauvegarderQuestionnaire(this,this.fenetreConcepteur));
                questionnaire_save.getItems().addAll(save_questionnaire,questionnaire_fini,delete_questionnaire);
                this.questionnaire_save.setPadding(new Insets(19, 0, 19, 0));
                this.questionnaire_save.setTooltip(new Tooltip(listInfoBulle.get(cpt)));
                leftAsside.getChildren().addAll(this.questionnaire_save);
                this.questionnaire_save.setStyle("-fx-background-color : #2498D9; -fx-cursor:hand;");
                cpt += 1;
            }else{
                Button bouton = new Button("", image);
                bouton.setTooltip(new Tooltip(listInfoBulle.get(cpt)));
                bouton.setStyle("-fx-background-color : #2498D9; -fx-cursor:hand;");
                bouton.setPadding(new Insets(19, 0, 19, 0));
                leftAsside.getChildren().add(bouton);
                cpt += 1;
            }
        }
        leftAsside.getChildren().add(this.btnDeconnexion);
        return leftAsside;}

    
    public ListePanels getModelPanel(){
        return this.listePanels;
    }
    
    

    

    public static void main(String[] args) {
        launch(args);
    }
    public void mettreAjourAffichage() {
        this.fenetreConcepteur.afficheQuestionnaire(chargerSondagePourConcepteur());
    }
    

    
    public void questionnaire_fini(Questionnaire questionnaire) {
        questionnaire.setEtat('A');
        RequeteSQL.updateQuestionnaireFiniSondeur(this.laConnexion,questionnaire);

    }

    public void questionnaire_fini_concepteur(Questionnaire questionnaire){
        questionnaire.setEtat('A');
        RequeteSQL.updateQuestionnaireFiniConcepteur(this.laConnexion,questionnaire);
    }

    

   
  

}