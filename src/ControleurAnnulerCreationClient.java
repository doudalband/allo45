import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurAnnulerCreationClient implements EventHandler<ActionEvent> {
    private FenetreConcepteur fenetreConcepteur;
    public ControleurAnnulerCreationClient(FenetreConcepteur fenetreConcepteur) {
        this.fenetreConcepteur = fenetreConcepteur;
    }

    @Override
    public void handle(ActionEvent arg0) {
        this.fenetreConcepteur.setCentreAjouterQuestionnaire();
        
    }

}
