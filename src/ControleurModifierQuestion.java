import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class ControleurModifierQuestion implements EventHandler<ActionEvent> {

    private FenetreConcepteur fenetreConcepteur;
    private Question question;

    public ControleurModifierQuestion(FenetreConcepteur fenetreConcepteur, Question question) {
        this.fenetreConcepteur = fenetreConcepteur;
        this.question = question;
    }

    @Override
    public void handle(ActionEvent event) {

        int indice =  this.fenetreConcepteur.getQuestionnaireCourant().getListeQuestions().indexOf(question);
        this.fenetreConcepteur.getBtnValiderQuestion().fire();
        this.fenetreConcepteur.getQuestionnaireCourant().getListeQuestions().set(indice, this.fenetreConcepteur.getQuestionnaireCourant().getListeQuestions().get(this.fenetreConcepteur.getQuestionnaireCourant().getListeQuestions().size()-1));
        this.fenetreConcepteur.getQuestionnaireCourant().getListeQuestions().remove(this.fenetreConcepteur.getQuestionnaireCourant().getListeQuestions().size()-1);
        this.fenetreConcepteur.miseAjourAffichage();
        

    }

        
    

}
