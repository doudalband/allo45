import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
// import javafx.beans.binding.Bindings;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.scene.layout.TilePane;
import javafx.scene.image.ImageView;


public class FenetreAnalyste extends BorderPane{

    private List<VBox> lesQuestiosnModifiables;
    private VBox right;
    private VBox sondages;
    private Questionnaire questionnaireCourant;
    private ListeClients modeleClient;
    private List<String> nomClients;
    private SplitPane sp;
    private BorderPane centre;
    private Connexion laConnexion;
    private GridPane lesQuestions;
    private ScrollPane centreQuestions;
    private HBox titre;
    private Label titreQuestionnaire;
    private TilePane reponseTilePane;


    public FenetreAnalyste(ListeClients modeleClient, Connexion laConnexion){
        super();
        this.reponseTilePane = new TilePane();
        this.centre = new BorderPane();
        this.laConnexion = laConnexion;
        this.lesQuestions  = new GridPane();
        this.sp = new SplitPane();
        this.questionnaireCourant = null;
        this.modeleClient = modeleClient;
        this.lesQuestiosnModifiables = new ArrayList<>();
        this.nomClients = new ArrayList<>();
        this.sondages = new VBox();
        this.right = right();
        this.sp.setOrientation(Orientation.HORIZONTAL);
        this.centreQuestions = creationScroll();
        this.lesQuestions.setAlignment(Pos.TOP_CENTER);  

        this.sp.setDividerPositions(0.831);
        this.sp.setStyle("-fx-background-color : #badfdd");
        this.setCenter(sp);
        this.centre = new BorderPane();
        this.centre.setCenter(centreQuestions);
        this.titreQuestionnaire = new Label("Questionnaire");
        this.titre = titre();
        this.centre.setTop(this.titre);
        this.sp.getItems().addAll(this.centre, this.right);
    }


    // public void start(){
    //     BorderPane root = new BorderPane();
    //     BorderPane top = this.top();
    //     VBox center = this.center();
    //     TilePane right = right();
    //     root.setTop(top);
    //     root.setCenter(center);
    //     root.setRight(right);
        
    // }

    private ScrollPane creationScroll() {
        VBox vbbbb = new VBox(this.lesQuestions);
        ScrollPane scrollP = new ScrollPane();
        scrollP.setVbarPolicy(javafx.scene.control.ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollP.setFitToHeight(true);
        scrollP.setFitToWidth(true);
        scrollP.setContent(vbbbb);
        vbbbb.setAlignment(Pos.TOP_CENTER);
        vbbbb.setPadding(new Insets(15));
        vbbbb.setStyle("-fx-background-color : #e2f0f8 ;");
        return scrollP;
    }


    public BorderPane top(){
        BorderPane top = new BorderPane();
        Label textTop = new Label("Allo45 - Module Analyste");
        top.setLeft(textTop);
        textTop.setStyle("-fx-font-size : 200%; -fx-font-weight: bold;");
        textTop.setPadding(new Insets(8));
        top.setStyle("-fx-background-color : #1560bd");
        return top;
    }



    private void miseAjourLesQuestions() {
        this.lesQuestions.getChildren().clear();
        Questionnaire questionnaire = this.getQuestionnaireCourant();
        int cpt = 0;
        this.lesQuestions.setAlignment(Pos.TOP_CENTER);
        if(questionnaire != null){
            
            for(Question question : questionnaire.getListeQuestions()){
                cpt++;
                    Label numQuest = new Label(cpt+" - ");
                    numQuest.setOnMouseClicked(new ControleurQuestionCliquee(this,question));
                    numQuest.setFont(Font.font("Arial",20));
                    Label texteQuestion = new Label(question.getTexteQ());
                    texteQuestion.setFont(Font.font("Arial",20));
                    texteQuestion.setOnMouseClicked(new ControleurQuestionCliquee(this,question));
                    List<Character> listeTypeRep = new ArrayList<>(Arrays.asList('l','m', 'c', 'u','n'));
                    List<String> listeReponse  = new ArrayList<>(Arrays.asList("Réponse libre", "Choix multiples", "Classement", "Réponse unique", "Notes"));            
                    Label typeRep = new Label(listeReponse.get(listeTypeRep.indexOf(question.getTypeReponse())));
                    typeRep.setStyle("-fx-padding : 8 12 8 12;");
                    typeRep.setFont(Font.font("Arial",20));
                    typeRep.setOnMouseClicked(new ControleurQuestionCliquee(this,question));
                    this.lesQuestions.add(numQuest, 0, cpt);
                    this.lesQuestions.add(texteQuestion, 1, cpt);
                    this.lesQuestions.add(typeRep, 2, cpt);
            } 
        }

    }

    public VBox center(){
        VBox center = new VBox(15);
        center.setPadding(new Insets(20));
        HBox boxCenter = new HBox(10);
        Label titre2 = new Label("Analyse du sondage sur les habitudes alimentaires");
        titre2.setStyle("-fx-font-size : 150%");

        //création du comboBox
        ComboBox<String> combo = new ComboBox<>();
        List<String> liste = new ArrayList<>();
        liste.add("Pie");
        liste.add("Line Graphs");
        liste.add("Histograms");
        liste.add("Bar Graphs");        
        ObservableList<String> liste2 = FXCollections.observableList(liste); 
        combo.setItems(liste2); // atribution de la liste au ComboBox
        combo.setValue("Pie"); // Element de la liste à afficher de base

        // création du PieChart (Graphique)
        PieChart chart = new PieChart ();
        chart.setTitle("Que lisez - vous au petit déjeuner ?");
        chart.getData().setAll(
            new PieChart.Data("Le journal ", 21) ,
            new PieChart.Data("Un livre ", 3) ,
            new PieChart.Data("Le courier ", 7) ,
            new PieChart.Data("La boîte de céréales ", 75));
        chart.setLegendSide(Side.LEFT); // pour mettre la lé gende à gauche

        // bouton suivant et précédent
        Button boutonCenter1 = new Button("Quetion précédente", new ImageView("back.png"));
        Button boutonCenter2 = new Button("Question suivante", new ImageView("next.png"));
        boxCenter.getChildren().addAll(boutonCenter1, boutonCenter2);
        center.getChildren().addAll(titre2, combo, chart, boxCenter);
        return center;
    }

    public VBox right(){
        VBox toulemid = new VBox();
        //

        HBox hbTitre = new HBox(10);
        Label titreH = new Label("Sujets / Thèmes");
        titreH.setFont(new Font("Arial", 20));
        titreH.setTextFill(Color.WHITE);
        titreH.setStyle("-fx-font-weight: bold");
        hbTitre.getChildren().add(titreH);
        hbTitre.setStyle("-fx-background-color : #2498D9; -fx-background-radius : 20px 20px 0 0;");
        hbTitre.setPadding(new Insets(10));


        VBox asideRight = new VBox();
        asideRight.setStyle("-fx-background-color : #e2f0f8 ;");
        asideRight.setMinSize(80, 80);
        asideRight.setPadding(new Insets(10));

        // Boite de tri des questionnaires réalisé(s)
        VBox trie = new VBox(5);
        trie.setPadding(new Insets(10));
        trie.setStyle("-fx-background-color : #b6d8eb");
        Label titre = new Label("Trier par :");
        titre.setStyle("-fx-font-weight: bold;");
        HBox trierParEntreprise = new HBox();
        Label entreprise = new Label("• Entreprise ");
        HBox trierParDate = new HBox();
        Label date = new Label("• Date ");
        ToggleGroup tg = new ToggleGroup();    
        RadioButton triEntreprise = new RadioButton();
        RadioButton triDate = new RadioButton();
        triEntreprise.setToggleGroup(tg);
        triDate.setToggleGroup(tg);
        triDate.setSelected(true);
        trierParEntreprise.getChildren().addAll(entreprise, triEntreprise);
        trierParDate.getChildren().addAll(date, triDate);
        trie.getChildren().addAll(titre, trierParEntreprise, trierParDate);
        
        triDate.setOnAction(new ControleurTrieDate(this));
        triEntreprise.setOnAction(new ControleurTriEntreprise(this));
    

       
        toulemid.getChildren().addAll(trie);
        toulemid.setSpacing(10);

        //
        asideRight.getChildren().addAll(hbTitre, toulemid,sondages);
        asideRight.setSpacing(10);
        asideRight.setPadding(new Insets(10));



        
        return asideRight;

    }

    public List<VBox> getLesQuestionsModifiables(){
        return this.lesQuestiosnModifiables;
    }

    public void afficheQuestionnaire(Map<Questionnaire, Client> listeQuestionnaire){
        this.sondages.getChildren().clear();
        this.lesQuestiosnModifiables.clear();
        for(Questionnaire questionnaire : listeQuestionnaire.keySet()){
            VBox vb = new VBox();
            
            vb.setStyle("-fx-cursor:hand;");
            this.lesQuestiosnModifiables.add(vb);
            
            vb.setOnMouseClicked(new ControleurCliqueQuestionnaireDroite(this,questionnaire,vb));
            TextFlow flow = new TextFlow();
            Text raisonsocial = new Text(listeQuestionnaire.get(questionnaire).getRaisonSociale() + "\n");
            raisonsocial.setStyle("-fx-font-weight: bold");

            Text text = new Text(questionnaire.getIdQuestionnaire() + " - ");
            Text text2 = new Text(questionnaire.getNomQuestionnaire());
            


            text.setFont(Font.font("Arial",20));
            raisonsocial.setFont(Font.font("Arial",20));
            text2.setFont(Font.font("Arial",20));

            flow.getChildren().addAll(text, raisonsocial, text2);
            vb.getChildren().add(flow);
            vb.setPadding(new Insets(10));
            this.sondages.getChildren().addAll(vb);
        }
        
    }


    public void trieQuestionnaireParDate() {
        
        List<Node> liste = (List<Node>) this.sondages.getChildren();
        List<Node> res = new ArrayList<>(liste);
        Collections.sort(res, new TrierQuestionnaireDate());
        this.sondages.getChildren().clear();
        for(Node n : res){
            this.sondages.getChildren().add(n);
        }
    }

    public void trieQuestionnaireParEntreprise() {
        List<Node> liste = (List<Node>) this.sondages.getChildren();
        List<Node> res = new ArrayList<>(liste);
        Collections.sort(res, new TrierQuestionnaireEntreprise());
        this.sondages.getChildren().clear();
        for(Node n : res){
            this.sondages.getChildren().add(n);
        }
    }
    
    public void clearCouleur(){
        for(VBox vb : this.lesQuestiosnModifiables){
            vb.setStyle("-fx-background-color: transparent; -fx-cursor:hand;");
            for(Node node : vb.getChildren()){
                TextFlow textflow = (TextFlow) node;
                for(Node text : textflow.getChildren()){
                    Text texte = (Text) text;
                    texte.setFill(Color.BLACK);

                }
            }
        }
    }



    public void miseAjourAffichage(){
        if(this.questionnaireCourant != null){
            this.titreQuestionnaire.setText("Questionnaire " + this.questionnaireCourant.getNomQuestionnaire());
        }
        this.nomClients();
        this.nomPanel();
        miseAjourLesQuestions();
        
    }
    public void afficheLesQuestions(){
        this.centre.setCenter(this.centreQuestions);
    }

    public HBox titre(){
        HBox hbTitre = new HBox(10);
        
        this.titreQuestionnaire.setFont(new Font("Arial", 35));
        this.titreQuestionnaire.setTextFill(Color.WHITE);
        this.titreQuestionnaire.setStyle("-fx-font-weight: bold;");
        hbTitre.getChildren().add(this.titreQuestionnaire);
        hbTitre.setStyle("-fx-background-color : #2498D9; -fx-background-radius : 20px 20px 0 0;");
        hbTitre.setPadding(new Insets(10));
        return hbTitre;
    }



    public Questionnaire getQuestionnaireCourant() {
        return this.questionnaireCourant;
    }

    public void setQuestionnaireCourant(Questionnaire q){
        this.questionnaireCourant = q;
    }

    public void nomClients() {
        this.nomClients.clear();
        for(Client client : this.modeleClient.getListeClient()){
            this.nomClients.add(client.getRaisonSociale());   
        }
    }


    public void nomPanel() {
    }


    public void afficheFenetreStatsQuestion(Question question) {
        VBox affichage = new VBox(20);
        affichage.setAlignment(Pos.CENTER);
        
        affichage.setStyle("-fx-background-color : #e2f0f8 ;");
        this.reponseTilePane.getChildren().clear();
        Button retour = new Button("Retour");
        retour.setOnAction(new ControleurBoutonRetourAnalyste(this));
       
        this.centre.setCenter(affichage);
        if(question.getTypeReponse().equals('l')){
            Label titreQuestion = new Label("" + question.getTexteQ());
            titreQuestion.setWrapText(true);
            titreQuestion.setTextAlignment(TextAlignment.CENTER);
            titreQuestion.setFont(Font.font("Arial",FontWeight.BOLD, 32));
            Text texte = new Text("Les réponses du sondage sont :");
            texte.setFont(Font.font("Arial",26));
            affichage.getChildren().addAll(titreQuestion,texte);
            this.afficheReponseLibre(question);
        }
        else if(question.getTypeReponse().equals('u') || question.getTypeReponse().equals('m') || question.getTypeReponse().equals('n')){
            Label titreQuestion = new Label("" + question.getTexteQ());
            titreQuestion.setWrapText(true);
            titreQuestion.setTextAlignment(TextAlignment.CENTER);
            titreQuestion.setFont(Font.font("Arial",FontWeight.BOLD, 32));
            Text texte = new Text("Les réponses du sondage sont :");
            texte.setFont(Font.font("Arial",26));
            affichage.getChildren().addAll(titreQuestion,texte);
            this.afficheReponseUNM(question);

        }else{
            Label titreQuestion = new Label("" + question.getTexteQ());
            titreQuestion.setWrapText(true);
            titreQuestion.setTextAlignment(TextAlignment.CENTER);
            titreQuestion.setFont(Font.font("Arial",FontWeight.BOLD, 32));
            Text texte = new Text("Voici le classement :");
            texte.setFont(Font.font("Arial",26));
            affichage.getChildren().addAll(titreQuestion,texte);
            this.afficheClassement(question);
        }
        affichage.getChildren().addAll(this.reponseTilePane, retour);

    }


    private void afficheClassement(Question question) {
        Statement s;
        Map<String, Integer> mapClassement = new HashMap<>();
        try {
            this.laConnexion.connecter();
            s = this.laConnexion.createStatement();
            ResultSet reponse = s.executeQuery("select valeur compteur from REPONDRE where numQ = " + question.getIdQ() + " and idQ=" + this.questionnaireCourant.getIdQuestionnaire()+ " group by valeur");
            GridPane gridepane = new GridPane();
            gridepane.setVgap(20);
            gridepane.setHgap(30);
            Label labelPlace = new Label("Place");
            Label labelRéponse = new Label("Réponse");
            gridepane.add(labelPlace, 0, 0,2,1);
            gridepane.add(labelRéponse, 1, 0,2,1);
            while(reponse.next()){
                int point = 3;
                String[] classement = reponse.getString(1).replace(" ","").split(";");
                for(String reponsepossible : classement){
                    if(mapClassement.get(reponsepossible) == null)
                        mapClassement.put(reponsepossible, point);
                    else
                    mapClassement.put(reponsepossible, mapClassement.get(reponsepossible)+point);
                    point--;
                }
            
            }
            List<Map.Entry<String, Integer> > list = new LinkedList<Map.Entry<String, Integer> >(mapClassement.entrySet());

     // Sort the list
     Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {

         public int compare(Map.Entry<String, Integer> o1,
                            Map.Entry<String, Integer> o2)
         {
             return (o2.getValue()).compareTo(o1.getValue());
         }
     });
        
    
            int cpt = 0;
            for(Entry<String,Integer> reponsepossible : list){
                cpt++;
                Text place = new Text(cpt+ "");
                place.setFont(Font.font("Arial",20));
                
                ResultSet reponse12 = s.executeQuery("select * from VALPOSSIBLE where numQ = " + question.getIdQ() + " and idQ= " + this.questionnaireCourant.getIdQuestionnaire()+ " and idV = " + Integer.parseInt(reponsepossible.getKey()));
                reponse12.next();
                String valuer = reponse12.getString(4);
                gridepane.add(place, 0, cpt);
                Text reponseQuestion = new Text(valuer);
                reponseQuestion.setFont(Font.font("Arial",20));
                gridepane.add(reponseQuestion, 1, cpt);
            }
                    
        
            this.reponseTilePane.setAlignment(Pos.CENTER);
            this.reponseTilePane.getChildren().add(gridepane);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void afficheReponseUNM(Question question) {
        Statement s;
        try {
            this.laConnexion.connecter();
            s = this.laConnexion.createStatement();
            int cpt = 0;
            GridPane gridepane = new GridPane();
            gridepane.setVgap(20);
            gridepane.setHgap(30);
            Label labelChoix = new Label("Choix");
            Label labelNbrRep = new Label("Nombre de Réponse");
            gridepane.add(labelChoix, 0, cpt,2,1);
            gridepane.add(labelNbrRep, 1, cpt,2,1);
            if(!question.getTypeReponse().equals('n')){
                ResultSet reponse1 = s.executeQuery("select valeur,count(valeur) from REPONDRE where numQ = " + question.getIdQ() + " and idQ=" + this.questionnaireCourant.getIdQuestionnaire()+ " group by valeur order by valeur");
                if(question.getTypeReponse().equals('m')){
                    Map<String,Integer> rep = new HashMap<>();
                    while(reponse1.next()){
                        String[] lesrep = reponse1.getString(1).split(";");
                        for(String larep : lesrep){
                            ResultSet reponse = s.executeQuery("select valeur, count(valeur) from VALPOSSIBLE where numQ = " + question.getIdQ() + " and idQ=" + this.questionnaireCourant.getIdQuestionnaire()+ " and idV = " + larep + " group by valeur");
                            reponse.next();
                            if(rep.containsKey(reponse.getString(1))){
                                rep.put(reponse.getString(1), rep.get(reponse.getString(1))+1);
                            }else{
                                rep.put(reponse.getString(1),1);
                            }
                        }
                    }
                    for(String dicorep : rep.keySet()){
                        cpt++;
                        Text choix = new Text(dicorep);
                        Text nbr = new Text(rep.get(dicorep) + "");
                        choix.setFont(Font.font("Arial",20));
                        nbr.setFont(Font.font("Arial",20));
                        gridepane.add(choix, 0, cpt);
                        gridepane.add(nbr, 1, cpt);
                    }
                
                }else{
                    while(reponse1.next()){
                        cpt++;
                        ResultSet reponse = s.executeQuery("select * from VALPOSSIBLE where numQ = " + question.getIdQ() + " and idQ=" + this.questionnaireCourant.getIdQuestionnaire()+ " and idV = " + reponse1.getString(1));
                        reponse.next();
                        Text choix = new Text(reponse.getString(4));
                        choix.setTextAlignment(TextAlignment.CENTER);
                        Text nbr = new Text(reponse1.getInt(2)+ "");
                        nbr.setTextAlignment(TextAlignment.CENTER);

                        choix.setFont(Font.font("Arial",20));
                        nbr.setFont(Font.font("Arial",20));
                    
                        gridepane.add(choix, 0, cpt);
                        gridepane.add(nbr, 1, cpt);
                    }
                    
                }
            }
            else{
                ResultSet reponse = s.executeQuery("select valeur, count(valeur) from REPONDRE where numQ = " + question.getIdQ() + " and idQ=" + this.questionnaireCourant.getIdQuestionnaire()+ " group by valeur order by valeur");
                while(reponse.next()){
                    cpt++;
                    Text choix = new Text(reponse.getString(1));
                    choix.setTextAlignment(TextAlignment.CENTER);
                    Text nbr = new Text(reponse.getString(2)+ "");
                    nbr.setTextAlignment(TextAlignment.CENTER);

                    choix.setFont(Font.font("Arial",20));
                    nbr.setFont(Font.font("Arial",20));
                
                    gridepane.add(choix, 0, cpt);
                    gridepane.add(nbr, 1, cpt);
                    
                }
            }
        
            this.reponseTilePane.setAlignment(Pos.CENTER);
            this.reponseTilePane.getChildren().add(gridepane);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    


    private void afficheReponseLibre(Question question) {

        Statement s;
        try {
            s = this.laConnexion.createStatement();
            ResultSet reponse = s.executeQuery("select * from REPONDRE where numQ = " + question.getIdQ() + " and idQ=" + this.questionnaireCourant.getIdQuestionnaire());
            while(reponse.next()){
                Text reponseText = new Text("" + reponse.getString(4) + ";");
                reponseText.setFont(Font.font("Arial",15));
                this.reponseTilePane.setAlignment(Pos.CENTER);
                this.reponseTilePane.getChildren().add(reponseText);
            }

            

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
            

    }
    
}