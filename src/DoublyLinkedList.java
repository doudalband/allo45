import java.util.NoSuchElementException;

public class DoublyLinkedList{

    private Noeud premierElement;
    private Noeud derniereElement;
    private int tailleListe;
    
    public DoublyLinkedList() {
        tailleListe = 0;
    }
    /**
     * this class keeps track of each element information
     * @author java2novice
     *
     */
    private class Noeud {
        Client clientCourant;
        Noeud clientSuiv;
        Noeud clientPrec;

        public Noeud(Client clientCourant, Noeud clientSuiv, Noeud clientPrec) {
            this.clientCourant = clientCourant;
            this.clientSuiv = clientSuiv;
            this.clientPrec = clientPrec;
        }

        public Client getClientCourant() {
            return clientCourant;
        }

        public Noeud getClientPrec() {
            return clientPrec;
        }

        public Noeud getClientSuiv() {
            return clientSuiv;
        }

        public void setClientPrec(Noeud clientPrec) {
            this.clientPrec = clientPrec;
        }

        public void setClientSuiv(Noeud clientSuiv) {
            this.clientSuiv = clientSuiv;
        }
    }
    /**
     * retourne la taille de la liste doublement chaîné
     * @return
     */
    public int size() { 
        return tailleListe;
    }
    
    /**
     * retourne true si la liste est vide et false sinon 
     * @return
     */
    public boolean isEmpty() { return tailleListe == 0; }
    
    /**
     * adds element at the starting of the linked list
     * @param element
     */
    public void addFirst(Client client) {
        Noeud tmp = new Noeud(client, premierElement, null);
        if(premierElement != null ) {premierElement.clientPrec = tmp;}
        premierElement = tmp;
        if(derniereElement == null) { derniereElement = tmp;}
        tailleListe++;
        System.out.println("adding: " + client);
    }
    
    /**
     * adds element at the end of the linked list
     * @param element
     */
    public void addLast(Client client) {

        Noeud tmp = new Noeud(client, null, derniereElement);
        if(derniereElement != null) {derniereElement.clientSuiv = tmp;}
        derniereElement = tmp;
        if(premierElement == null) { premierElement = tmp;}
        tailleListe++;
        System.out.println("adding: "+ client);
    }
    
    /**
     * this method walks forward through the linked list
     */
    public void iterateForward(){
    
        System.out.println("iterating forward..");
        Noeud tmp = premierElement;
        while(tmp != null){
            tmp = tmp.clientSuiv;
        }
    }

    public void supprimeClient(Client client){
        Noeud tmp = premierElement;
        while(! tmp.getClientCourant().equals(client)){
            tmp = tmp.clientSuiv;
        }
        tmp.clientPrec.setClientSuiv(tmp.getClientSuiv()); 
        tmp.clientSuiv.setClientPrec(tmp.getClientPrec()); 
    }

    /**
     * this method walks backward through the linked list
     */
    public void iterateBackward(){
        System.out.println("iterating backword..");
        Noeud tmp = derniereElement;
        while(tmp != null){
            System.out.println(tmp.clientCourant);
            tmp = tmp.clientPrec;
        }
    }

    /**
     * this method removes element from the start of the linked list
     * @return
     */
    public Client removeFirst(){
        if (tailleListe == 0){
            throw new NoSuchElementException();
        }
        Noeud tmp = premierElement;
        premierElement = premierElement.clientSuiv;
        premierElement.clientPrec = null;
        tailleListe--;
        System.out.println("deleted: "+tmp.clientCourant);
        return tmp.clientCourant;
    }
    
    /**
     * this method removes element from the end of the linked list
     * @return
     */
    public Client removeLast() {
        if (tailleListe == 0) throw new NoSuchElementException();
        Noeud tmp = derniereElement;
        derniereElement = derniereElement.clientPrec;
        derniereElement.clientSuiv = null;
        tailleListe--;
        System.out.println("deleted: "+tmp.clientCourant);
        return tmp.clientCourant;
    }
}